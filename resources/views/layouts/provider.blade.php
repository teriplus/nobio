<!DOCTYPE html>

<html direction="rtl" dir="rtl" style="direction: rtl">

<head>
    <base href="">
    <meta charset="utf-8" />

    <title>پنل ارائه دهنده | @yield("title")</title>

    <meta name="description" content="Updates and statistics" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link href="{{ asset('dashboard/plugins/custom/fullcalendar/fullcalendar.bundle.rtl.css?v=7.0.6') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/plugins/global/plugins.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('dashboard/plugins/custom/prismjs/prismjs.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('dashboard/css/style.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('dashboard/css/themes/layout/header/base/light.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('dashboard/css/themes/layout/header/menu/light.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('dashboard/css/themes/layout/brand/dark.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('dashboard/css/themes/layout/aside/dark.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('dashboard/plugins/custom/kanban/kanban.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link rel="shortcut icon" href="{{ asset('dashboard/media/logos/favicon.ico') }}" />

</head>


<body id="kt_body"
    class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">


    <livewire:provider.component.mobile-header>

        <div class="d-flex flex-column flex-root">
            <div class="d-flex flex-row flex-column-fluid page">
                <livewire:provider.component.aside>
                    <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                        <livewire:provider.component.header>
                            {{ $slot }}
                            {{-- <livewire:admin.component.footer> --}}
                    </div>
            </div>
        </div>

        <livewire:component.user-panel>
            <livewire:provider.component.scrolltop>

                <script>
                    var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
                </script>

                <script>
                    var KTAppSettings = {
                        "breakpoints": {
                            "sm": 576,
                            "md": 768,
                            "lg": 992,
                            "xl": 1200,
                            "xxl": 1400
                        },
                        "colors": {
                            "theme": {
                                "base": {
                                    "white": "#ffffff",
                                    "primary": "#3699FF",
                                    "secondary": "#E5EAEE",
                                    "success": "#1BC5BD",
                                    "info": "#8950FC",
                                    "warning": "#FFA800",
                                    "danger": "#F64E60",
                                    "light": "#E4E6EF",
                                    "dark": "#181C32"
                                },
                                "light": {
                                    "white": "#ffffff",
                                    "primary": "#E1F0FF",
                                    "secondary": "#EBEDF3",
                                    "success": "#C9F7F5",
                                    "info": "#EEE5FF",
                                    "warning": "#FFF4DE",
                                    "danger": "#FFE2E5",
                                    "light": "#F3F6F9",
                                    "dark": "#D6D6E0"
                                },
                                "inverse": {
                                    "white": "#ffffff",
                                    "primary": "#ffffff",
                                    "secondary": "#3F4254",
                                    "success": "#ffffff",
                                    "info": "#ffffff",
                                    "warning": "#ffffff",
                                    "danger": "#ffffff",
                                    "light": "#464E5F",
                                    "dark": "#ffffff"
                                }
                            },
                            "gray": {
                                "gray-100": "#F3F6F9",
                                "gray-200": "#EBEDF3",
                                "gray-300": "#E4E6EF",
                                "gray-400": "#D1D3E0",
                                "gray-500": "#B5B5C3",
                                "gray-600": "#7E8299",
                                "gray-700": "#5E6278",
                                "gray-800": "#3F4254",
                                "gray-900": "#181C32"
                            }
                        },
                        "font-family": "Poppins"
                    };
                </script>

                <script src="{{ asset('dashboard/plugins/global/plugins.bundle.js?v=7.0.6') }}"></script>
                <script src="{{ asset('dashboard/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.6') }}"></script>
                <script src="{{ asset('dashboard/js/scripts.bundle.js?v=7.0.6') }}"></script>
                <script src="{{ asset('dashboard/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.6') }}"></script>
                <script src="{{ asset('dashboard/js/pages/widgets.js?v=7.0.6') }}"></script>
                <script src="{{ asset('dashboard/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.6') }}"></script>

                @livewireScripts
</body>

</html>
