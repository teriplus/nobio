<!DOCTYPE html>

<html direction="rtl" dir="rtl" style="direction: rtl">

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>نوبیو | @yield("title")</title>
    <meta name="description" content="Updates and statistics" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link href="{{ asset('web/plugins/custom/fullcalendar/fullcalendar.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('web/plugins/global/plugins.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('web/plugins/custom/prismjs/prismjs.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('web/css/style.bundle.rtl.css?v=7.0.6') }}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{ asset('web/media/logos/favicon.ico') }}" />

    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
            text-align: left;
            direction: ltr;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
            text-align: left;
            direction: ltr;
        }

    </style>
    @livewireStyles
</head>


<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled page-loading">

    <livewire:web.component.mobile-header>

        <div class="d-flex flex-column flex-root">
            <div class="d-flex flex-row flex-column-fluid page">
                <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                    <livewire:web.component.header>
                        {{ $slot }}
                        {{-- <livewire:web.component.footer > --}}
                </div>
            </div>
        </div>

        <livewire:web.component.scrolltop>

            <script>
                var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
            </script>
            <script>
                var KTAppSettings = {
                    "breakpoints": {
                        "sm": 576,
                        "md": 768,
                        "lg": 992,
                        "xl": 1200,
                        "xxl": 1200
                    },
                    "colors": {
                        "theme": {
                            "base": {
                                "white": "#ffffff",
                                "primary": "#0BB783",
                                "secondary": "#E5EAEE",
                                "success": "#1BC5BD",
                                "info": "#8950FC",
                                "warning": "#FFA800",
                                "danger": "#F64E60",
                                "light": "#F3F6F9",
                                "dark": "#212121"
                            },
                            "light": {
                                "white": "#ffffff",
                                "primary": "#D7F9EF",
                                "secondary": "#ECF0F3",
                                "success": "#C9F7F5",
                                "info": "#EEE5FF",
                                "warning": "#FFF4DE",
                                "danger": "#FFE2E5",
                                "light": "#F3F6F9",
                                "dark": "#D6D6E0"
                            },
                            "inverse": {
                                "white": "#ffffff",
                                "primary": "#ffffff",
                                "secondary": "#212121",
                                "success": "#ffffff",
                                "info": "#ffffff",
                                "warning": "#ffffff",
                                "danger": "#ffffff",
                                "light": "#464E5F",
                                "dark": "#ffffff"
                            }
                        },
                        "gray": {
                            "gray-100": "#F3F6F9",
                            "gray-200": "#ECF0F3",
                            "gray-300": "#E5EAEE",
                            "gray-400": "#D6D6E0",
                            "gray-500": "#B5B5C3",
                            "gray-600": "#80808F",
                            "gray-700": "#464E5F",
                            "gray-800": "#1B283F",
                            "gray-900": "#212121"
                        }
                    },
                    "font-family": "Poppins"
                };
            </script>

            <script src="{{ asset('web/plugins/global/plugins.bundle.js?v=7.0.6') }}"></script>
            <script src="{{ asset('web/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.6') }}"></script>
            <script src="{{ asset('web/js/scripts.bundle.js?v=7.0.6') }}"></script>

            <script src="{{ asset('web/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.6') }}"></script>

            <script src="{{ asset('web/js/pages/widgets.js?v=7.0.6') }}"></script>

            @livewireScripts
</body>

</html>
