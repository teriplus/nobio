@section('title', 'ورود کاربران')

<div>
    <div class="content d-flex flex-column flex-column-fluid col-sm-12 col-md-4 mx-auto" id="kt_content">
        <livewire:web.component.sub-header title="ورود کاربران">
            <div class="d-flex flex-column-fluid">
                <div class="container">
                    <div class="card card-custom">
                        <form wire:submit.prevent='{{ $submit }}'>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>شماره موبایل:</label>
                                    <input wire:model.defer='mobile_number' type="number"
                                        class="form-control number @error('mobile_number') is-invalid @enderror"
                                        placeholder="0919409xxxx"
                                        {{ $submit == 'verify' ? 'disabled' : 'autofocus' }} />
                                    @error('mobile_number')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted">ما هرگز شماره‌ی شما را با هیچ کس دیگری به اشتراک
                                        نمی گذاریم.</span>
                                </div>
                                @if ($submit == 'verify')
                                    <div class="form-group">
                                        <label>کد تایید:</label>
                                        <input wire:model.defer='auth_code' type="number"
                                            class="form-control number @error('auth_code') is-invalid @enderror"
                                            placeholder="1234" autofocus />
                                        @error('auth_code')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <span class="form-text text-muted">کد تایید فقط ۵ دقیقه اعتبار دارد.</span>
                                    </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary spinner-white spinner-right mr-2"
                                    wire:loading.class='spinner'>ارسال</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
</div>
