<div>
    <div class="header-top">
        <div class=" container ">

            <div class="d-none d-lg-flex align-items-center mr-3">
                <a href="{{ route('web.home') }}" class="mr-10">
                    <img alt="Logo" src="{{ asset('web/media/logos/logo-letter-5.png') }}" class="max-h-35px" />
                </a>
            </div>

            <div class="topbar">
                @auth
                    @role('admin')
                        <div class="topbar-item">
                            <a href="{{ route('admin.dashboard') }}" class="btn btn-sm btn-danger ml-2">پنل مدیریت</a>
                        </div>
                    @endrole

                    @role('provider')
                        <div class="topbar-item">
                            <a href="{{ route('provider.dashboard') }}" class="btn btn-sm btn-outline-warning ml-2">پنل
                                ارائه‌دهنده</a>
                        </div>
                    @else
                        <div class="topbar-item">
                            <a href="{{ route('web.convert') }}" class="btn btn-sm btn-info ml-2">ارتقا به
                                ارائه‌دهنده</a>
                        </div>
                        @endif
                        {{-- <livewire:web.component.notification-toolbar> --}}
                        <div class="topbar-item ml-2">
                            <div class="btn btn-icon w-auto d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                                <div class="d-flex text-right pr-3">
                                    <span
                                        class="text-white opacity-50 font-weight-bold font-size-sm d-none d-md-inline mr-1">{{ auth()->user()->first_name ? 'سلام، ' : 'خوش‌آمدید' }}
                                    </span>
                                    <span
                                        class="text-white font-weight-bolder font-size-sm d-none d-md-inline">{{ auth()->user()->first_name ?? '' }}</span>
                                </div>
                                <span class="symbol symbol-35">
                                    <span
                                        class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-15">{{ substr(auth()->user()->first_name, 0, 2) ?: 'ب' }}</span>
                                </span>
                            </div>
                        </div>
                        <livewire:component.user-panel>
                        @endauth
                        @guest
                            <div class="topbar-item">
                                <a href="{{ route('auth.register') }}" class="btn btn-primary">ورود / عضویت</a>
                            </div>
                        @endguest
                </div>

            </div>
        </div>
    </div>
