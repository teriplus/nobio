<div>
    <div class="header-bottom">
        <!--begin::Container-->
        <div class=" container ">
            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <!--begin::Header Menu-->
                <div id="kt_header_menu"
                    class="header-menu header-menu-left header-menu-mobile  header-menu-layout-default ">
                    <!--begin::Header Nav-->
                    <ul class="menu-nav ">
                        <li class="menu-item  menu-item-submenu menu-item-rel" data-menu-toggle="click"
                            aria-haspopup="true"><a href="javascript:;" class="menu-link menu-toggle"><span
                                    class="menu-text">اپلیکیشن
                                    ها</span><span class="menu-desc"></span><i class="menu-arrow"></i></a>
                            <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                <ul class="menu-subnav">
                                    <li class="menu-item  menu-item-submenu" data-menu-toggle="hover"
                                        aria-haspopup="true"><a href="javascript:;" class="menu-link menu-toggle"><span
                                                class="svg-icon menu-icon">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/ارتباطات/نشانی-card.svg--><svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z"
                                                            fill="#000000" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span><span class="menu-text">کاربران</span><i
                                                class="menu-arrow"></i></a>
                                        <div class="menu-submenu menu-submenu-classic menu-submenu-right">
                                            <ul class="menu-subnav">
                                                <li class="menu-item " aria-haspopup="true"><a
                                                        href="custom/apps/user/list-default.html"
                                                        class="menu-link "><i class="menu-bullet menu-bullet-dot">
                                                            <span></span></i>
                                                        <span class="menu-text">لیست - پیش فرض
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <!--end::Header Nav-->
                </div>
                <!--end::Header Menu-->
            </div>
            <!--end::Header Menu Wrapper-->

            <!--begin::Desktop جستجو-->
            <div class="d-none d-lg-flex align-items-center">
                <div class="quick-search quick-search-inline ml-4 w-250px" id="kt_quick_search_inline">
                    <!--begin::Form-->
                    <form method="get" class="quick-search-form">
                        <div class="input-group rounded">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="svg-icon svg-icon-lg">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/عمومی/جستجو.svg--><svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                            viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24" />
                                                <path
                                                    d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                    fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                <path
                                                    d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                    fill="#000000" fill-rule="nonzero" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span> </span>
                            </div>

                            <input type="text" class="form-control h-40px" placeholder="جستجو..." />

                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="quick-search-close ki ki-close icon-sm"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->

                    <!--begin::جستجو Toggle-->
                    <div id="kt_quick_search_toggle" data-toggle="dropdown" data-offset="0px,0px">
                    </div>
                    <!--end::جستجو Toggle-->

                    <!--begin::دراپ دان-->
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg dropdown-menu-anim-up">
                        <div class="quick-search-wrapper scroll" data-scroll="true" data-height="350"
                            data-mobile-height="200">
                        </div>
                    </div>
                    <!--end::دراپ دان-->
                </div>
            </div>
            <!--end::Desktop جستجو-->
        </div>
        <!--end::Container-->
    </div>
</div>
