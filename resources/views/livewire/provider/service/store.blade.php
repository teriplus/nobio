@section('title', 'اضافه کردن خدمت جدید')

<div>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-3 mr-5">
                        اضافه کردن خدمت جدید </h5>
                </div>
            </div>
        </div>

        <div class="d-flex flex-column-fluid">
            <div class=" container ">
                <div class="card card-custom">
                    <div class="card-header">
                        <h3 class="card-title">
                            اطلاعات خدمات جدید
                        </h3>
                    </div>
                    <!--begin::Form-->
                    <form class="form" wire:submit.prevent='add'>
                        <div class="card-body">

                            <div class="alert alert-custom alert-outline-info fade show mb-5" role="alert">
                                <div class="alert-icon"><i class="flaticon-info"></i></div>
                                <div class="alert-text">سعی کنید زمان تقریبی ارائه خدمت را با دقت وارد کنید، نوبت
                                    ‌دهی شما طبق این فیلد تنظیم خواهد شد.</div>
                                <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-label="نزدیک">
                                        <span aria-hidden="true"><i class="ki ki-close"></i></span>
                                    </button>
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-lg-3 mb-3">
                                    <label>نام خدمت:</label>
                                    <input wire:model.defer='name' type="text"
                                        class="form-control @error('name') is-invalid @enderror"
                                        placeholder="نام خدمت را وارد کنید" autofocus />
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-lg-2 mb-3">
                                    <label>زمان تقریبی ارائه خدمت:</label>
                                    <input wire:model.defer='time' type="number" dir="ltr"
                                        class="form-control @error('time') is-invalid @enderror" placeholder="10" />
                                    @error('time')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted">زمان را به دقیقه وارد کنید.</span>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="description">توضیحات:</label>
                                <textarea wire:model.defer='description'
                                    class="form-control @error('description') is-invalid @enderror" id="description"
                                    rows="2"> xxx </textarea>
                                @error('description')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary mr-2"
                                        wire:load.attr='disabled'>ثبت</button>
                                    <button type="reset" class="btn btn-secondary">بازنشانی</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>
