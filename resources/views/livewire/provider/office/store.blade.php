@section('title', 'اضافه کردن دفتر جدید')

<div>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-3 mr-5">
                        اضافه کردن دفتر جدید </h5>
                </div>
            </div>
        </div>



        <div class="d-flex flex-column-fluid">
            <div class=" container ">
                @if ($service_items->isEmpty())
                    <div class="alert alert-custom alert-light-dark fade show mb-5" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">
                            لطفا ابتدا خدمات مجموعه خود را در بخش
                            <a href="{{ route('provider.service.store') }}" class="font-size-strong">خدمات ما</a>
                            ثبت کنید.
                        </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="نزدیک">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @else
                    <div class="card card-custom">
                        <div class="card-header">
                            <h3 class="card-title">
                                اطلاعات دفتر جدید
                            </h3>
                        </div>
                        <!--begin::Form-->
                        <form class="form" wire:submit.prevent='add'>
                            <div class="card-body">
                                <div class="form-group row">

                                    <div class="col-lg-3 mb-3">
                                        <label>نام دفتر:</label>
                                        <input wire:model.defer='name' type="text"
                                            class="form-control @error('name') is-invalid @enderror"
                                            placeholder="نام دفتر خود را وارد کنید" autofocus />
                                        @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="col-lg-2 mb-3">
                                        <label for="city">شهر:</label>
                                        <select wire:model.defer='city'
                                            class="form-control @error('city') is-invalid @enderror" id="city">
                                            <option>انتخاب</option>
                                            <option value="1">تهران</option>
                                            <option value="2">کرج</option>
                                            <option value="3">مشهد</option>
                                            <option value="4">اصفهان</option>
                                        </select>
                                        @error('city')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="description">توضیحات:</label>
                                    <textarea wire:model.defer='description' class="form-control @error('description') is-invalid @enderror"
                                        id="description" rows="2">{{ $description }}</textarea>
                                    @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="address">آدرس:</label>
                                    <textarea wire:model.defer='address' class="form-control @error('address') is-invalid @enderror" id="address"
                                        rows="2">{{ $address }}</textarea>
                                    @error('address')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group row">

                                    <div class="col-lg-3 mb-3">
                                        <label>شماره موبایل‌های دفتر:</label>
                                        @for ($i = 1; $i <= $mobile_numbers_count; $i++)
                                            <input wire:model.defer='mobile_numbers.{{ $i }}' type="number"
                                                dir="ltr"
                                                class="form-control mt-3 @error('mobile_numbers*') is-invalid @enderror"
                                                placeholder="09123456789" />
                                        @endfor
                                        @error('mobile_numbers*')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <a class="btn btn-clean font-weight-bold btn-sm mt-3"
                                            wire:click='addNewMobileNumber'>
                                            <i class="ki ki-plus icon-sm"></i>
                                            اضافه کن
                                        </a>
                                    </div>

                                    <div class="col-lg-3 mb-3">
                                        <label>شماره تلفن‌های دفتر:</label>
                                        @for ($i = 0; $i < $phone_numbers_count; $i++)
                                            <input wire:model.defer='phone_numbers.{{ $i }}' type="number"
                                                dir="ltr"
                                                class="form-control mt-3 @error('phone_numbers*') is-invalid @enderror"
                                                placeholder="7736xxxx" />
                                        @endfor
                                        @error('phone_numbers*')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <a class="btn btn-clean font-weight-bold btn-sm mt-3"
                                            wire:click='addNewPhoneNumber'>
                                            <i class="ki ki-plus icon-sm"></i>
                                            اضافه کن
                                        </a>
                                    </div>

                                    <div class="col-lg-3 mb-3">
                                        <label>شبکه‌های اجتماعی:</label>
                                        <input wire:model.defer='telegram' type="text" dir="ltr"
                                            class="form-control mb-2 @error('telegram') is-invalid @enderror"
                                            placeholder="telegram_id" />
                                        <input wire:model.defer='instagram' type="text" dir="ltr"
                                            class="form-control @error('instagram') is-invalid @enderror"
                                            placeholder="instagram_id" />
                                        @error('telegram')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        @error('instagram')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="col-lg-3 mb-3">
                                        <label>خدمات:</label>
                                        <div class="checkbox-list @error('service_items') is-invalid @enderror">
                                            @foreach ($service_items as $i => $item)
                                                <label class="checkbox checkbox-lg">
                                                    <input wire:model.defer='service_items.{{ $i }}.value'
                                                        type="checkbox" value="{{ $item['id'] }}" />
                                                    <span></span>
                                                    {{ $item['name'] }}
                                                </label>
                                            @endforeach
                                        </div>
                                        @error('service_items')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>

                            </div>

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary mr-2"
                                            wire:load.attr='disabled'>ثبت</button>
                                        <button type="reset" class="btn btn-secondary">بازنشانی</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
