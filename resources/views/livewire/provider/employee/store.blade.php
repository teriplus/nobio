@section('title', 'اضافه کردن کارمند جدید')

<div>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-3 mr-5">
                        اضافه کردن کارمند جدید </h5>
                </div>
            </div>
        </div>

        <div class="d-flex flex-column-fluid">
            <div class=" container ">
                @if ($items->isEmpty())
                    <div class="alert alert-custom alert-light-dark fade show mb-5" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">
                            برای ثبت کارمند می‌بایست حداقل یک خدمت و یک دفتر ثبت شده باشد.
                        </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="نزدیک">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @else
                    <div class="card card-custom">
                        <div class="card-header">
                            <h3 class="card-title">
                                اطلاعات کارمند جدید
                            </h3>
                        </div>
                        <!--begin::Form-->
                        <form class="form" wire:submit.prevent='add'>
                            <div class="card-body">

                                @error('office')
                                    <livewire:component.toast color="danger" :message="$message" />
                                @enderror

                                @error('service')
                                    <livewire:component.toast color="danger" :message="$message" />
                                @enderror

                                <div class="form-group row">

                                    <div class="col-lg-3 mb-3">
                                        <label>نام کارمند:</label>
                                        <input wire:model.defer='label' type="text"
                                            class="form-control @error('label') is-invalid @enderror"
                                            placeholder="نام کارمند را وارد کنید" autofocus />
                                        @error('label')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="col-lg-3 mb-3">
                                        <label>شماره موبایل:</label>
                                        <input wire:model.defer='mobile_number' type="number" dir="ltr"
                                            class="form-control @error('mobile_number') is-invalid @enderror"
                                            placeholder="شماره موبایل کارمند را وارد کنید" />
                                        @error('mobile_number')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>

                                @foreach ($items as $i => $office)
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">
                                            {{ $office['name'] }}
                                        </label>
                                        <div class="col-3">
                                            <span class="switch switch-outline switch-icon switch-primary">
                                                <label>
                                                    <input wire:model='items.{{ $i }}.value'
                                                        value="{{ $office['id'] }}" type="checkbox" checked="checked"
                                                        name="select">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>

                                    @if ($office['value'])
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">مهارت‌ها</label>
                                            <div class="col-9 col-form-label">
                                                <div class="checkbox-list">

                                                    @foreach ($office['services'] as $j => $service)
                                                        <label class="checkbox checkbox-lg checkbox-primary">
                                                            <input type="checkbox"
                                                                wire:model.defer='items.{{ $i }}.services.{{ $j }}.value'
                                                                name="service_{{ $i }}_{{ $j }}"
                                                                value="{{ $service['id'] }}">
                                                            <span></span>
                                                            {{ $service['name'] }}
                                                        </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($items->count() - 1 != $i)
                                        <div class="separator separator-dashed mb-8"></div>
                                    @endif
                                @endforeach
                            </div>

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary mr-2"
                                            wire:load.attr='disabled'>ثبت</button>
                                        <button type="reset" class="btn btn-secondary">بازنشانی</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
