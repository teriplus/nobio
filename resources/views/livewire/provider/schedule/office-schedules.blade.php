@section('title', 'برنامه زمانی')

<div>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                        برنامه زمانی </h5>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">

                @if (count($employee_items) == 0)
                    <div class="alert alert-custom alert-light-dark fade show mb-5" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">
                            لطفا ابتدا حداقل یک کارمند را در بخش
                            <a href="{{ route('provider.employee.list') }}" class="font-size-strong">منابع انسانی</a>
                            به این دفتر منسوب کنید.
                        </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="نزدیک">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @else
                    <div class="card card-custom mb-5">
                        <div class="card-body" wire:target='search' wire:loading.class='filter'>
                            <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded"
                                id="kt_datatable" style="">
                                <table class="datatable-table overflow-auto" style="display: block;">
                                    <thead class="datatable-head">
                                        <tr class="datatable-row" style="left: 0px;">
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">شنبه</span>
                                            </th>
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">یکشنبه</span>
                                            </th>
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">دوشنبه</span>
                                            </th>
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">سه شنبه</span>
                                            </th>
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">چهارشنبه</span>
                                            </th>
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">پنجشنبه</span>
                                            </th>
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">جمعه</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="datatable-body">
                                        @foreach ($weeks as $week_index => $week)
                                            <tr data-row="{{ $week_index }}" class="datatable-row"
                                                style="left: 0px;">
                                                @foreach ($week as $day_index => $day)
                                                    <td data-field="{{ $day['format'] }}"
                                                        aria-label="{{ $day['format'] }}" class="datatable-cell">
                                                        <button type="button"
                                                            wire:click='is_schedulable({{ json_encode($day) }})'
                                                            class="btn {{ $day['status'] ? 'btn-outline-primary' : 'btn-secondary' }}"
                                                            style="width: 110px;">
                                                            {{ $day['format'] }}
                                                        </button>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center mt-3">
                                <a wire:click='increaseWeekLimit()' class="btn btn-icon btn-light-primary btn-sm"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ki ki-plus icon-1x"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($schedulable_type == 'possible')
                    <div class="row">
                        <div class="col-sm-12 col-lg-8">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        {{ $scheduling['description'] }}
                                    </h3>
                                </div>

                                @if (session()->has('message'))
                                    @foreach (session()->get('message') as $color => $message)
                                        <livewire:component.toast wire:key="{{ $loop->index }}" :color="$color"
                                            :message="$message" />
                                    @endforeach
                                @endif

                                <form wire:submit.prevent='add'>

                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label class="col-form-label text-right">
                                                زمان شروع
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <div class="input-group timepicker">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-clock-o"></i>
                                                        </span>
                                                    </div>
                                                    <input
                                                        class="form-control @error('start_time') is-invalid @enderror"
                                                        placeholder="09:30" wire:model.defer='start_time' dir="ltr">
                                                    <div class="valid-feedback">
                                                        با موفقبت انجام شد
                                                    </div>
                                                    @error('start_time')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}</div>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label text-right">
                                                زمان پایان
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <div class="input-group timepicker">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-clock-o"></i>
                                                        </span>
                                                    </div>
                                                    <input class="form-control @error('end_time') is-invalid @enderror"
                                                        placeholder="12:30" wire:model.defer='end_time' dir="ltr">
                                                    <div class="valid-feedback">
                                                        با موفقبت انجام شد
                                                    </div>
                                                    @error('end_time')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-6 col-sm-12 mb-3">
                                                <label>کارمندان:</label>
                                                <div class="checkbox-list">
                                                    @foreach ($employee_items as $i => $item)
                                                        <label
                                                            class="checkbox checkbox-lg @error('employee_items') is-invalid @enderror">
                                                            <input type="checkbox"
                                                                wire:model.defer='employee_items.{{ $i }}.value' />
                                                            <span></span>
                                                            {{-- <a href="{{ route('provider.employee.edit', ['employee_id' => $item['id']]) }}" target="_blank">{{ $item['label'] }}</a> --}}
                                                            {{ $item['label'] }}
                                                        </label>
                                                    @endforeach
                                                    @error('employee_items')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <button type="submit" wire:load.attr='disabled'
                                                class="btn btn-primary mr-2">ثبت</button>
                                            <button wire:click.prevent='hideScheduling()'
                                                class="btn btn-secondary">لغو</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4 pt-3">
                            {{-- schedule_history --}}
                            @if ($schedule_history->count())
                                <div class="card card-custom card-stretch gutter-b">
                                    <div class="card-header border-0">
                                        <h3 class="card-title font-weight-bolder text-dark">
                                            {{ $schedule_history->count() }}
                                            برنامه
                                            زمانی فعال
                                        </h3>
                                    </div>

                                    <div class="card-body pt-2">
                                        @foreach ($schedule_history as $schedule)
                                            <div class="d-flex align-items-center mt-10">
                                                <span class="bullet bullet-bar bg-info align-self-stretch mr-5"></span>

                                                <div class="d-flex flex-column flex-grow-1">
                                                    <a class="text-dark-75 font-weight-bold font-size-lg mb-1">
                                                        {{ $schedule->getTime('start', ':') }}
                                                        تا
                                                        {{ $schedule->getTime('end', ':') }}
                                                    </a>
                                                    <span class="text-muted font-weight-bold">
                                                        شناسه: {{ $schedule->id }}
                                                    </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @elseif ($schedulable_type == 'impossible')
                    <div class="row">
                        <div class="col-sm-12 col-lg-8">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        {{ $scheduling['description'] }}
                                    </h3>
                                </div>

                                @if (session()->has('message'))
                                    @foreach (session()->get('message') as $color => $message)
                                        <livewire:component.toast wire:key="{{ $loop->index }}" :color="$color"
                                            :message="$message" />
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
