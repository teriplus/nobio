<div>
    @section('title', 'انتخاب دفتر')

    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-3 mr-5">
                        انتخاب دفتر </h5>
                </div>
            </div>
        </div>

        <div class="d-flex flex-column-fluid">
            <div class=" container ">

                <div class="card card-custom">

                    <div class="card-header">
                        <h3 class="card-title">
                            دفتر را انتخاب کنید
                        </h3>
                    </div>

                    <div class="card-body">
                        <div class="mb-7">
                            <div class="row align-items-center">

                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="input-icon">
                                                <input wire:model.defer='search' wire:keydown.enter='search' type="text"
                                                    class="form-control" placeholder="جستجو..."
                                                    id="kt_datatable_search_query">
                                                <span><i class="flaticon2-search-1 text-muted"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        @if ($items->isEmpty())
                            <div class="alert alert-custom alert-light-dark fade show mb-5" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">هیچ دفتری برای نمایش وجود ندارد.</div>
                                <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-label="نزدیک">
                                        <span aria-hidden="true"><i class="ki ki-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="form-group m-0">
                                <div class="row">
                                    @foreach ($items as $office)
                                        <div class="col-lg-6"
                                            wire:click='redirectToOfficeSchedules({{ $office->id }})'>
                                            <label class="option">
                                                <span class="option-control">
                                                    <span class="radio">
                                                        <input type="radio" name="radio" />
                                                        <span></span>
                                                    </span>
                                                </span>
                                                <span class="option-label">
                                                    <span class="option-head">
                                                        <span class="option-title">
                                                            {{ $office->name }}
                                                        </span>
                                                        <span class="option-focus">
                                                            {{ $office->city() }}
                                                        </span>
                                                    </span>
                                                    <span class="option-body">
                                                        {{ substr($office->description, 0, 50) . '...' }}
                                                    </span>
                                                </span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
