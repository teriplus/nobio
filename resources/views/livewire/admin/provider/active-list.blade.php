@section('title', 'مشتریان فعال')

<div>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                        مشتریان فعال </h5>
                </div>
            </div>
        </div>

        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body" wire:target='search'
                    wire:loading.class='filter'>
                        <div class="mb-7">
                            <div class="row align-items-center">

                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="input-icon">
                                                <input wire:model.defer='search' wire:keydown.enter='search' type="text"
                                                    class="form-control" placeholder="جستجو..."
                                                    id="kt_datatable_search_query">
                                                <span><i class="flaticon2-search-1 text-muted"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        @if ($items->isEmpty())
                            <div class="alert alert-custom alert-light-dark fade show mb-5" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">هیچ رکوردی برای نمایش وجود ندارد.</div>
                                <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-label="نزدیک">
                                        <span aria-hidden="true"><i class="ki ki-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded"
                                id="kt_datatable" style="">
                                <table class="datatable-table overflow-auto" style="display: block;">
                                    <thead class="datatable-head">
                                        <tr class="datatable-row" style="left: 0px;">
                                            <th data-field="User_id" class="datatable-cell datatable-cell-sort"><span
                                                    style="width: 110px;">شناسه مشتری</span></th>
                                            <th data-field="Full_name" class="datatable-cell datatable-cell-sort"><span
                                                    style="width: 110px;">نام</span></th>
                                            <th data-field="Mobile_number" class="datatable-cell datatable-cell-sort">
                                                <span style="width: 110px;">شماره موبایل</span>
                                            </th>
                                            <th data-field="Type" data-autohide-disabled="false"
                                                class="datatable-cell datatable-cell-sort"><span
                                                    style="width: 110px;">نوع</span></th>
                                            <th data-field="Actions" data-autohide-disabled="false"
                                                class="datatable-cell datatable-cell-sort"><span
                                                    style="width: 125px;">اکشن</span></th>
                                        </tr>
                                    </thead>

                                    <tbody class="datatable-body">
                                        @foreach ($items as $i => $user)
                                            <tr data-row="{{ $i }}" class="datatable-row"
                                                style="left: 0px;">
                                                <td data-field="User_id" aria-label="{{ $user->id }}"
                                                    class="datatable-cell">
                                                    <span style="width: 110px;">{{ $user->id }}</span>
                                                </td>
                                                <td data-field="Full_name" aria-label="{{ $user->last_name }}"
                                                    class="datatable-cell">
                                                    <span style="width: 110px;">{{ $user->last_name }}</span>
                                                </td>
                                                <td data-field="Mobile_number" aria-label="{{ $user->mobile_number }}"
                                                    class="datatable-cell">
                                                    <span style="width: 110px;">{{ $user->mobile_number }}</span>
                                                </td>
                                                <td data-field="Type" data-autohide-disabled="false" aria-label="2"
                                                    class="datatable-cell"><span style="width: 110px;"><span
                                                            class="label label-success label-dot mr-2"></span><span
                                                            class="font-weight-bold text-success">free</span></span>
                                                </td>
                                                <td data-field="Actions" data-autohide-disabled="false"
                                                    aria-label="null" class="datatable-cell">
                                                    <span style="overflow: visible; position: relative; width: 125px;">
                                                        <a href="#" class="btn btn-sm btn-clean btn-icon mr-2"
                                                            title="ورود به پنل مشتری">
                                                            <span class="svg-icon svg-icon-md">
                                                                <i class="icon-xl la la-door-open"></i>
                                                            </span>
                                                        </a>
                                                        {{-- <a href="{{ route('admin.provider.with-draw', ['id' => $user->id]) }}}"
                                                        class="btn btn-sm btn-clean btn-icon mr-2"
                                                        title="حدف مشتری">
                                                        <span class="svg-icon svg-icon-md re">
                                                            <i class="icon-xl la la-trash"></i>
                                                        </span>
                                                    </a> --}}
                                                    </span>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
