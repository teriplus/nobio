@section('title', 'درخواست‌ها')

<div>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-3 mr-5">
                        درخواست‌ها </h5>
                </div>
            </div>
        </div>

        <div class="d-flex flex-column-fluid">
            <div class=" container ">
                <div class="card card-custom">
                    <div class="card-header">
                        <h3 class="card-title">
                            بررسی درخواست
                        </h3>
                    </div>
                    <!--begin::Form-->
                    <form class="form" wire:submit.prevent='accept'>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-3 mb-3">
                                    <label>نام:</label>
                                    <input wire:model.defer='first_name' type="text"
                                        class="form-control @error('first_name') is-invalid @enderror"
                                        placeholder="نام خود را وارد کنید" />
                                    @error('first_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 mb-3">
                                    <label>نام خانوادگی:</label>
                                    <input wire:model.defer='last_name' type="text"
                                        class="form-control @error('last_name') is-invalid @enderror"
                                        placeholder="نام خانوادگی خود را وارد کنید" />
                                    @error('last_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 mb-3">
                                    <label>شماره موبایل:</label>
                                    <input wire:model.defer='mobile_number' type="number"
                                        class="form-control @error('mobile_number') is-invalid @enderror"
                                        placeholder="شماره موبایل را وارد کنید" disabled />
                                    <span class="form-text text-muted">مثال: 09194095098</span>
                                    @error('mobile_number')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 mb-3">
                                    <label>پست الکترونیکی:</label>
                                    <input wire:model.defer='email' type="email"
                                        class="form-control @error('email') is-invalid @enderror"
                                        placeholder="پست لکترونیکی خود را وارد کنید" />
                                    @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-3 mb-3">
                                    <label>کدملی:</label>
                                    <input wire:model.defer='national_code' type="number"
                                        class="form-control @error('national_code') is-invalid @enderror"
                                        placeholder="کدملی خود را وارد کنید" />
                                    @error('national_code')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 mb-3">
                                    <label>کدپستی:</label>
                                    <input wire:model.defer='zip_code' type="number"
                                        class="form-control @error('zip_code') is-invalid @enderror"
                                        placeholder="کدپستی خود را وارد کنید" />
                                    @error('zip_code')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 mb-3">
                                    <label>شماره تلفن:</label>
                                    <input wire:model.defer='phone_number' type="number"
                                        class="form-control @error('phone_number') is-invalid @enderror"
                                        placeholder="شماره تلفن را وارد کنید" />
                                    <span class="form-text text-muted">مثال: 77360000</span>
                                    @error('phone_number')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 mb-3">
                                    <label for="city">شهر:</label>
                                    <select wire:model.defer='city'
                                        class="form-control @error('city') is-invalid @enderror" id="city">
                                        <option>انتخاب</option>
                                        <option {{ $city == 1 ? 'selected' : '' }} value="1">تهران</option>
                                        <option {{ $city == 2 ? 'selected' : '' }} value="2">کرج</option>
                                        <option {{ $city == 3 ? 'selected' : '' }} value="3">مشهد</option>
                                        <option {{ $city == 4 ? 'selected' : '' }} value="4">اصفهان</option>
                                    </select>
                                    @error('city')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address">آدرس:</label>
                                <textarea wire:model.defer='address'
                                    class="form-control @error('address') is-invalid @enderror" id="address"
                                    rows="2">{{ $address }}</textarea>
                                @error('address')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary mr-2"
                                        wire:load.attr='disabled'>تایید</button>
                                    <button type="button" class="btn btn-secondary" wire:click='reject'>رد
                                        درخواست</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>
