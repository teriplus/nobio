<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("order_id")->unsigned();
            $table->tinyInteger("method")->comment("1:online 2:transfer");
            $table->tinyInteger("gateway")->nullable()->comment("1:zibal, 2:saman");
            $table->string("ref_num")->nullable();
            $table->string("amount")->nullable()->comment("tomans");
            $table->boolean("is_completed")->default(false);
            $table->timestamps();

            $table->foreign("order_id")->references("id")->on("orders")->onDelete("cascade");
        });

        DB::update('alter table payments AUTO_INCREMENT = 1000');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
