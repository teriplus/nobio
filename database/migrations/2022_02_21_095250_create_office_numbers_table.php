<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficeNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_numbers', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("office_id")->unsigned();
            $table->string("number");
            $table->tinyInteger("type")->comment("1: phone_number, 2: mobile_number");

            $table->foreign("office_id")->references("id")->on("offices")->onDelete("cascade");
            
            $table->unique(['office_id', 'number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_numbers');
    }
}
