<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_requests', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("user_id")->unsigned()->unique();
            $table->integer("accepted_by")->default(null)->nullable();
            $table->integer("rejected_by")->default(null)->nullable();
            $table->timestamp("accepted_at")->nullable();
            $table->timestamp("rejected_at")->nullable();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_requests');
    }
}
