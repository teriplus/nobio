<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_service', function (Blueprint $table) {
            // $table->increments("id");
            $table->integer("schedule_id")->unsigned();
            $table->integer("service_id")->unsigned();
            $table->tinyInteger("available")->unsigned();

            $table->foreign("service_id")->references("id")->on("services")->onDelete("cascade");
            $table->foreign("schedule_id")->references("id")->on("schedules")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_service');
    }
}
