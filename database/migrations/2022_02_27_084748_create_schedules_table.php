<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("schedulable_id")->unsigned();
            $table->string("schedulable_type");
            $table->integer("day")->unsigned()->comment("daykey");
            $table->integer("start_at")->unsigned()->comment("time_day");
            $table->integer("end_at")->unsigned()->comment("time_day");
            $table->timestamps();
        });

        DB::update('alter table schedules AUTO_INCREMENT = 1000');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
