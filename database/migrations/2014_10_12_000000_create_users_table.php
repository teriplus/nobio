<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("users", function (Blueprint $table) {
            $table->increments("id");
            $table->tinyInteger("status")->default(1)->comment("0: inactive, 1:active");
            $table->string("mobile_number", 11)->unique();
            $table->string("first_name")->nullable();
            $table->string("last_name")->nullable();
            $table->tinyInteger("gender")->nullable()->comment("1: Male, 2: Female");
            $table->string("email")->unique()->nullable();
            $table->string("birth_day", 8)->nullable();
            $table->tinyInteger("city")->nullable();
            $table->string("national_code", 12)->unique()->nullable();
            $table->string("zip_code", 10)->nullable();
            $table->string("phone_number", 8)->unique()->nullable();
            $table->string("address", 500)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::update('alter table users AUTO_INCREMENT = 1000');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("users");
    }
}
