<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_permission', function (Blueprint $table) {
            $table->integer("employee_id")->unsigned();
            $table->integer("office_id")->unsigned();
            $table->integer("service_id")->unsigned();

            $table->foreign("employee_id")->references("id")->on("employees")->onDelete("cascade");
            $table->foreign("office_id")->references("id")->on("offices")->onDelete("cascade");
            $table->foreign("service_id")->references("id")->on("services")->onDelete("cascade");

            $table->unique(['employee_id', 'office_id', 'service_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_permission');
    }
}
