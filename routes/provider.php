<?php

use Illuminate\Support\Facades\Route;

Route::get('/', \App\Http\Livewire\Provider\Dashboard::class)
	->name('provider.dashboard');

Route::group([
	'prefix' => "service"
], function () {

	Route::get('/store', \App\Http\Livewire\Provider\Service\Store::class)
		->name('provider.service.store');

	Route::get('{service_id}/edit', \App\Http\Livewire\Provider\Service\Edit::class)
		->name('provider.service.edit');

	Route::get('/list', \App\Http\Livewire\Provider\Service\ServiceList::class)
		->name('provider.service.list');
});

Route::group([
	'prefix' => "office"
], function () {

	Route::get('/store', \App\Http\Livewire\Provider\Office\Store::class)
		->name('provider.office.store');

	Route::get('{office_id}/edit', \App\Http\Livewire\Provider\Office\Edit::class)
		->name('provider.office.edit');

	Route::get('/list', \App\Http\Livewire\Provider\Office\OfficeList::class)
		->name('provider.office.list');
});

Route::group([
	'prefix' => "employee"
], function () {

	Route::get('/store', \App\Http\Livewire\Provider\Employee\Store::class)
		->name('provider.employee.store');

	Route::get('{employee_id}/edit', \App\Http\Livewire\Provider\Employee\Edit::class)
		->name('provider.employee.edit');

	Route::get('/list', \App\Http\Livewire\Provider\Employee\EmployeeList::class)
		->name('provider.employee.list');
});

Route::group([
	'prefix' => "schedule"
], function () {

	Route::get('/select-office', \App\Http\Livewire\Provider\Schedule\SelectOffice::class)
		->name('provider.schedule.select-office');

	Route::get('/office/{office_id}', \App\Http\Livewire\Provider\Schedule\OfficeSchedules::class)
		->name('provider.schedule.office-schedule');

	Route::get('/store', \App\Http\Livewire\Provider\Schedule\Store::class)
		->name('provider.schedule.store');

	Route::get('{schedule_id}/edit', \App\Http\Livewire\Provider\Schedule\Edit::class)
		->name('provider.schedule.edit');

	Route::get('/list', \App\Http\Livewire\Provider\Schedule\ScheduleList::class)
		->name('provider.schedule.list');
});
