<?php

use App\Http\Livewire\Web\Home;
use App\Http\Livewire\Auth\Register;
use Illuminate\Support\Facades\Route;

Route::get("/", \App\Http\Livewire\Web\Home::class)->name("web.home");

/**
 * Auth Routes
 */
Route::group(["middleware" => "guest"], function () {
	Route::get("/auth", \App\Http\Livewire\Auth\Register::class)->name("auth.register");
});

/**
 * User Routes
 */
Route::group([
	"middleware"	=> "auth",
	"prefix"		=> "user"
], function () {
	/**
	 * Logout
	 */
	Route::get("/logout", \App\Http\Livewire\Auth\Logout::class)->name("auth.logout");

	/**
	 * Profile
	 */
	Route::get("/profile", \App\Http\Livewire\Web\Profile::class)->name("web.profile");

	/**
	 * Convert To ProviderRequest
	 */
	Route::get("/convert", \App\Http\Livewire\Web\Convert::class)->name("web.convert");
});


Route::get("artisan", function (\Illuminate\Http\Request $request) {
	$exitCode = \Illuminate\Support\Facades\Artisan::call($request->input("cmd"));
	return response()->json($exitCode);
});

Route::get("tst", function (\Illuminate\Http\Request $request) {
	return \App\Models\Schedule::getNextWeeks();
});

