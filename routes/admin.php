<?php

use Illuminate\Support\Facades\Route;

Route::get('/', \App\Http\Livewire\Admin\Dashboard::class)->name('admin.dashboard');

/**
 * Providers
 */
Route::group([
	"prefix"        => "provider",
	"middleware"    => "can:provider.managment",
], function () {

	Route::get('/convert-list', \App\Http\Livewire\Admin\Provider\ConvertList::class)
		->name('admin.provider.convert-list')
		->middleware('can:provider.convert');

	Route::get('/convert/{id}', \App\Http\Livewire\Admin\Provider\ConvertRecord::class)
		->name('admin.provider.convert-request')
		->middleware('can:provider.convert');

	Route::get('/avtive-list', \App\Http\Livewire\Admin\Provider\ActiveList::class)
		->name('admin.provider.active-list')
		->middleware('can:provider.active');

	Route::get('/with-draw/{id}', \App\Http\Livewire\Admin\Provider\WithDraw::class)
		->name('admin.provider.with-draw')
		->middleware('can:provider.active');
});