<?php

namespace App\Listeners\Provider\AddNewOffice;

use App\Events\Provider\AddNewOffice;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InsertOffice
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\AddNewOffice  $event
	 * @return void
	 */
	public function handle(AddNewOffice $event)
	{
		$event->office = request()->user()->offices()->create([
			"name"			=> $event->office_info->name,
			"description"	=> $event->office_info->description,
			"address"		=> $event->office_info->address,
			"city"			=> $event->office_info->city,
			"instagram"		=> $event->office_info->instagram,
			"telegram"		=> $event->office_info->telegram,
		]);

	}
}
