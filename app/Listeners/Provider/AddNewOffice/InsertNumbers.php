<?php

namespace App\Listeners\Provider\AddNewOffice;

use App\Events\Provider\AddNewOffice;
use App\Models\Office;
use App\Models\OfficeNumber;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InsertNumbers
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\AddNewOffice  $event
	 * @return void
	 */
	public function handle(AddNewOffice $event)
	{
		$numbers	= [];
		$office_id	= $event->office->id;

		foreach ($event->office_info->phone_numbers as $phone_number) {
			$numbers[] = [
				"office_id"	=> $office_id,
				"number"	=> $phone_number,
				"type"		=> OfficeNumber::PHONE_TYPE
			];
		}

		foreach ($event->office_info->mobile_numbers as $mobile_number) {
			$numbers[] = [
				"office_id"	=> $office_id,
				"number"	=> $mobile_number,
				"type"		=> OfficeNumber::MOBILE_TYPE
			];
		}

		$event->office->numbers()->insert($numbers);
	}
}
