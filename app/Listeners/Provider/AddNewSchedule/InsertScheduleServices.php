<?php

namespace App\Listeners\Provider\AddNewSchedule;

use Illuminate\Support\Facades\DB;
use App\Events\Provider\AddNewSchedule;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InsertScheduleServices
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\AddNewSchedule  $event
	 * @return void
	 */
	public function handle(AddNewSchedule $event)
	{
		$office_id		= $event->schedule_info->office->id;
		$employee_ids	= $event->schedule_info->selected_employees;

		$employee_permission = DB::table('employee_permission')
			->where("office_id", $office_id)
			->whereIn("employee_id", $employee_ids)
			->get()
			->pluck("service_id")
			->toArray();

		$service_count = array_count_values($employee_permission);

		$infos = [];
		foreach ($service_count as $service_id => $count) {
			$infos[] = [
				"schedule_id"	=> $event->schedule->id,
				"service_id"	=> $service_id,
				"available"		=> $count,
			];
		}

		DB::table('schedule_service')->insert($infos);
	}
}
