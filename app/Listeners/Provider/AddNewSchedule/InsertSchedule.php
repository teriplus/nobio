<?php

namespace App\Listeners\Provider\AddNewSchedule;

use App\Events\Provider\AddNewSchedule;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InsertSchedule
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\AddNewSchedule  $event
	 * @return void
	 */
	public function handle(AddNewSchedule $event)
	{
		$event->schedule = $event->office->schedules()->create([
			"day"		=> $event->schedule_info->scheduling["daykey"],
			"start_at"	=> $event->schedule_info->start_at,
			"end_at"	=> $event->schedule_info->end_at,
		]);
	}
}
