<?php

namespace App\Listeners\Provider\AddNewService;

use App\Events\Provider\AddNewService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InsertService
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\AddNewService  $event
	 * @return void
	 */
	public function handle(AddNewService $event)
	{
		request()->user()->services()->create([
			"name"			=> $event->service_info["name"],
			"description"	=> $event->service_info["description"],
			"time"			=> $event->service_info["time"],
		]);
	}
}
