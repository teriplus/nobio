<?php

namespace App\Listeners\Provider\EditOffice;

use App\Events\Provider\EditOffice as EventEditOffice;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EditOffice
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\EventEditOffice  $event
	 * @return void
	 */
	public function handle(EventEditOffice $event)
	{
		$event->office->update([
			"name"			=> $event->office_info->name,
			"description"	=> $event->office_info->description,
			"address"		=> $event->office_info->address,
			"city"			=> $event->office_info->city,
			"instagram"		=> $event->office_info->instagram,
			"telegram"		=> $event->office_info->telegram,
		]);
	}
}
