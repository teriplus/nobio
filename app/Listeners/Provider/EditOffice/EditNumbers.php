<?php

namespace App\Listeners\Provider\EditOffice;

use App\Events\Provider\EditOffice as EventEditOffice;
use App\Models\Office;
use App\Models\OfficeNumber;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EditNumbers
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\EventEditOffice  $event
	 * @return void
	 */
	public function handle(EventEditOffice $event)
	{
		$event->office->numbers()->delete();

		$numbers	= [];
		$office_id	= $event->office->id;

		foreach ($event->office_info->phone_numbers as $phone_number) {
			$numbers[] = [
				"office_id"	=> $office_id,
				"number"	=> $phone_number,
				"type"		=> OfficeNumber::PHONE_TYPE
			];
		}

		foreach ($event->office_info->mobile_numbers as $mobile_number) {
			$numbers[] = [
				"office_id"	=> $office_id,
				"number"	=> $mobile_number,
				"type"		=> OfficeNumber::MOBILE_TYPE
			];
		}

		$event->office->numbers()->insert($numbers);
	}
}
