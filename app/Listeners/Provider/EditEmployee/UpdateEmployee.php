<?php

namespace App\Listeners\Provider\EditEmployee;

use App\Events\Provider\EditEmployee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateEmployee
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Provider\EditEmployee  $event
     * @return void
     */
    public function handle(EditEmployee $event)
    {
        $event->employee->update([
            "label" => $event->employee_info->label
        ]);
    }
}
