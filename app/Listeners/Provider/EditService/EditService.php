<?php

namespace App\Listeners\Provider\EditService;

use App\Events\Provider\EditService as EventEditService;
use App\Models\Log;
use App\Models\Service;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EditService
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\EventEditService  $event
	 * @return void
	 */
	public function handle(EventEditService $event)
	{
		$event->service->update([
			"name"			=> $event->service_info["name"],
			"description"	=> $event->service_info["description"],
			"time"			=> $event->service_info["time"],
		]);
	}
}
