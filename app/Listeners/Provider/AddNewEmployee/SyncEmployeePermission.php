<?php

namespace App\Listeners\Provider\AddNewEmployee;

use Illuminate\Support\Facades\DB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncEmployeePermission
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @return void
	 */
	public function handle($event)
	{
		$infos = [];

		$offices	= $event->employee_info->selected;

		$office_ids	= [];

		foreach ($offices as $office_id => $services) {
			foreach ($services as $service) {
				$info["employee_id"]	= $event->employee->id;
				$info["office_id"]		= $office_id;
				$info["service_id"]		= $service;
				$infos[] = $info;
			}
		}

		DB::table('employee_permission')
			->where("employee_id", $event->employee->id)
			->delete();

		DB::table('employee_permission')->insert($infos);
	}
}
