<?php

namespace App\Listeners\Provider\AddNewEmployee;

use App\Events\Provider\AddNewEmployee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpgradeToEmployee
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Provider\AddNewEmployee  $event
     * @return void
     */
    public function handle(AddNewEmployee $event)
    {
        $event->user->giveRolesTo(["employee"]);
    }
}
