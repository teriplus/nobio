<?php

namespace App\Listeners\Provider\AddNewEmployee;

use App\Events\Provider\AddNewEmployee;
use App\Models\Employee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InsertEmployee
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\Provider\AddNewEmployee  $event
	 * @return void
	 */
	public function handle(AddNewEmployee $event)
	{
		$event->employee = request()
			->user()
			->employees()
			->firstOrCreate([
				"user_id"	=> $event->user->id,
				"label"		=> $event->employee_info->label
			]);
	}
}
