<?php

namespace App\Listeners\Web\ConvertRequest;

use App\Events\Web\ConvertRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InsertToProviderRequest
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\ConvertRequest  $event
	 * @return void
	 */
	public function handle(ConvertRequest $event)
	{
		request()->user()->ProviderRequest()->firstOrCreate()->update([
			"accepted_by"		=> null,
			"accepted_at"		=> null,
			"rejected_by"	=> null,
			"rejected_at"	=> null
		]);
	}
}
