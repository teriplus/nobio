<?php

namespace App\Listeners\Web\UserRegister;

use App\Services\SmsPattern;
use App\Events\Web\UserRegister;
use App\Services\Notification\Notification;

class SendAuthCodeSms
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\UserRegister  $event
	 * @return void
	 */
	public function handle(UserRegister $event)
	{
		// SEND_SMS_TAG
		app(Notification::class)
			->send(
				$event->mobileNumber,
				["$event->authCode"],
				Notification::REGISTER
			);
	}
}
