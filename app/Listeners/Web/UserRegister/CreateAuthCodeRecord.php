<?php

namespace App\Listeners\Web\UserRegister;

use App\Models\AuthCode;
use App\Events\Web\UserRegister;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateAuthCodeRecord
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\Events\UserRegister  $event
	 * @return void
	 */
	public function handle(UserRegister $event)
	{

		AuthCode::where("mobile_number", $event->mobileNumber)->delete();
		AuthCode::create([
			"mobile_number"	=> $event->mobileNumber,
			"auth_code"		=> $event->authCode,
			"created_at"	=> now()
		]);
	}
}
