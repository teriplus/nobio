<?php

namespace App\Listeners\Admin\WithDrawProvider;

use App\Events\Admin\WithDrawProvider;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class WithDraw
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Admin\WithDrawProvider  $event
     * @return void
     */
    public function handle(WithDrawProvider $event)
    {
        $event->user->withDrawRoles(["provider"]);
    }
}
