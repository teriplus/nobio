<?php

namespace App\Listeners\Admin\RejectToProvider;

use App\Events\Admin\RejectToProvider;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MoveUserToRejectList
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Admin\RejectToProvider  $event
     * @return void
     */
    public function handle(RejectToProvider $event)
    {
        $event->user->ProviderRequest()->update([
            "rejected_by" => auth()->user()->id,
            "rejected_at" => now(),
            "accepted_by"  => null,
            "accepted_at"  => null
        ]);
    }
}
