<?php

namespace App\Listeners\Admin\ConvertToProvider;

use App\Services\SmsPattern;
use App\Events\Admin\AcceptToProvider;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\Notification\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\AcceptToProvider  $event
     * @return void
     */
    public function handle(AcceptToProvider $event)
    {
        // $list = [
        //     [
        //         "mobile"	=> $event->user->mobile_number,
        //         "text"		=> SmsPattern::acceptToProvider($event->user),
        //     ]
        // ];

        // app(Notification::class)->send($list);
    }
}
