<?php

namespace App\Listeners\Admin\ConvertToProvider;

use App\Events\Admin\AcceptToProvider;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GiveProviderRole
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\AcceptToProvider  $event
     * @return void
     */
    public function handle(AcceptToProvider $event)
    {
        $event->user->giveRolesTo(["provider"]);
    }
}
