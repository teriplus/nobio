<?php

namespace App\Listeners\Admin\ConvertToProvider;

use App\Events\Admin\AcceptToProvider;
use App\Models\ProviderRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateProviderRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\AcceptToProvider  $event
     * @return void
     */
    public function handle(AcceptToProvider $event)
    {
        $event->user->ProviderRequest()->update([
            "accepted_by" => auth()->user()->id,
            "accepted_at" => now(),
            "rejected_by" => null,
            "rejected_at" => null,
        ]);
    }
}
