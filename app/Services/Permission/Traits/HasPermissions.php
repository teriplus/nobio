<?php

namespace App\Services\Permission\Traits;

use App\Models\Permission;

trait HasPermissions
{
	public function permissions()
	{
		return $this->belongsToMany(Permission::class);
	}

	public function givePermissionsTo(array $permissions)
	{
		$permissions = $this->getAllPermissions($permissions);

		if ($permissions->isEmpty()) return $this;

		$this->permissions()->syncWithoutDetaching($permissions);

		return $this;
	}

	public function withDrawPermissions(array $permissions)
	{
		$permissions = $this->getAllPermissions($permissions);

		$this->permissions()->detach($permissions);

		return $this;
	}

	public function refreshPermissions(array $permissions)
	{
		$permissions = $this->getAllPermissions($permissions);

		$this->permissions()->sync($permissions);

		return $this;
	}

	public function hasPermission(Permission $permission): bool
	{
		return
			$this->hasPermissionThroughRole($permission) ||
			$this->permissions->contains($permission);
	}

	protected function hasPermissionThroughRole(Permission $permission)
	{
		foreach ($permission->roles as $role) {
			if ($this->roles->contains($role)) return true;
		}
		return false;
	}

	protected function getAllPermissions(array $permissions)
	{
		return Permission::whereIn("name", $permissions)->get();
	}
}
