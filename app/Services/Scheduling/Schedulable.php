<?php

namespace App\Services\Scheduling;

use Morilog\Jalali\Jalalian;

trait Schedulable
{
	public function getNextWeeks($next = 4)
	{
		$format = $format ?? env("DATE_FORMAT");
		$today = jdate()->now()->getDayOfWeek();

		$weeks = [];
		for ($i = $today * -1; $i <= $next * 7; $i++) {

			$date = jdate()->now()->addDays($i);

			$days[] = [
				"status"		=> self::getDayStatus($date),
				"format"		=> $date->format($format),
				"description"	=> $date->format("%A, %d %B"),
				"daykey"		=> $date->format("Ymd"),
			];

			if ($date->addDays(1)->isStartOfWeek()) {
				$weeks[] = $days;
				$days = [];
			}
		}

		return $weeks;
	}

	public static function getDayStatus(Jalalian $j): bool
	{
		return $j->isFuture() || $j->isToday();
	}
}
