<?php

namespace App\Services\Notification\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\RequestException;

class MeliPayamak
{
	private $url;

	public function __construct($url, Client $client)
	{
		$this->url      = $url;
		$this->client   = $client;
	}

	public function send($mobile_number, $args, $body_id)
	{
		if (!env("PRODUCTION"))
			return;

		$data = array('bodyId' => (int)$body_id, 'to' => (string) $mobile_number, 'args' => $args);

		$url = $this->url;
		$data_string = json_encode($data);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		// Next line makes the request absolute insecure
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Use it when you have trouble installing local issuer certificate
		// See https://stackoverflow.com/a/31830614/1743997

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		$result = curl_exec($ch);
		curl_close($ch);
		// to debug
		// if(curl_errno($ch)){
		//     echo 'Curl error: ' . curl_error($ch);
		// }
	}
}
