<?php

namespace App\Services\Notification;

use Illuminate\Support\Manager;
use App\Services\Notification\Providers\MeliPayamak;

class Notification extends Manager
{
	public const REGISTER = 77733;

	public function getDefaultDriver()
	{
		return "meliPayamak";
	}

	public function createMeliPayamakDriver()
	{
		app()->singleton(MeliPayamak::class);

		return resolve(MeliPayamak::class, [
			"url" => env("MELIPAYAMAK_URL")
		]);
	}
}
