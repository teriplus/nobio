<?php

namespace App\Services;

class Helper
{
	/**
	 * Trim mix data struct
	 */
	public static function trimmer($input)
	{
		if (!is_array($input)) {
			return self::trimmerCore($input);
		}

		foreach ($input as $index => $value) {
			if (is_array($value)) {
				$value = self::trimmer($value);
			} else {
				/**
				 * Core logic
				 */
				$value = self::trimmerCore($value);
			}

			$trim[$index] = $value;
		}

		return $trim;
	}

	private static function trimmerCore($string)
	{
		$value = trim($string);
		return $value;
	}

	public static function intToEnglish($string)
	{
		$en_num = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", '10', '0', '8', '9', '2', '1', '3', '7', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '6', '6', "4", "5");
		$fa_num = array("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", '١٠', '٠', '٨', '٩', '٢', '١', '٣', '٧', '٣', '٣', '٣', '٣', '٣', '٣', '٣', '٣', '٣', '٣', '٣', '٣', '٦', '٦', '٤', '٥');
		return str_replace($fa_num, $en_num, $string);
	}
}
