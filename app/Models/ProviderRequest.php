<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProviderRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id", "accepted_at", "accepted_by", "rejected_at", "rejected_by"
    ];

    // ---------------- relations ---------------- //

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // ---------------- instance methods ---------------- //

    public function status()
    {
        if ($this->rejected_by != null) {
            return "REJECTED";
        }

        if ($this->accepted_by != null) {
            return "ACCEPTED";
        }

        return "NEW";
    }

    // ---------------- static methods ---------------- //

    public static function hasUserActiveRequest(User $user): bool
    {
        return $user->ProviderRequest()->whereNull("rejected_by")->whereNull("accepted_by")->exists();
    }
}
