<?php

namespace App\Models;

use App\Models\User;
use App\Models\Office;
use App\Models\Employee;
use App\Models\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Service extends Model
{
	use HasFactory;

	public $timestamps = false;

	protected $fillable = [
		"name",
		"description",
		"time",
	];

	// ---------------- relations ---------------- //

	public function user()
	{
		return $this->hasOne(User::class);
	}

	public function schedule()
	{
		return $this->hasOne(Schedule::class);
	}

	public function offices()
	{
		return $this->belongsToMany(Office::class, "office_service");
	}

	public function employees()
	{
		return $this->belongsToMany(Employee::class, "employee_permission");
	}

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //

	public static function normalizeArrayToCreateServices(array $services): array
	{
		$list = [];
		foreach ($services as $service) {
			$list[] = [$service];
		}

		return $list;
	}
}
