<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthCode extends Model
{
	use HasFactory;

	public $timestamps = false;

	protected $fillable = [
		"mobile_number",
		"auth_code",
		"created_at"
	];

	// ---------------- relations ---------------- //

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //
	
	public static function generateAuthCode($mobile_number): int
	{
		try {
			$auth = AuthCode::where("mobile_number", $mobile_number)->firstOrFail();
		} catch (\Throwable $th) {
			$auth = AuthCode::create([
				"mobile_number"	=> $mobile_number,
				"auth_code"		=> env("PRODUCTION") ? rand(1111, 9999) : 1234
			]);
		}

		return $auth->auth_code;
	}

	public static function verifyAuthCode($mobile_number, $auth_code): string
	{
		try {
			$auth = AuthCode
				::where("mobile_number", $mobile_number)
				->firstOrFail();
		} catch (\Throwable $th) {
			return "failed";
		}

		if ($auth->created_at < now()->subMinutes(5)) {
			return "expired";
		}

		if ($auth->auth_code != $auth_code) {
			return "failed";
		}

		$auth->delete();
		return "success";
	}
}
