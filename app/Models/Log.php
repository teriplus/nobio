<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	use HasFactory;

	public $timestamps = false;

	protected $fillable = [
		"ip_address",
		"action_id",
		"target_id",
		"created_at",
	];

	private static $actions = [
		/**
		 * Admin
		 */
		\App\Events\Admin\AcceptToProvider::class	=> 1000,
		\App\Events\Admin\RejectToProvider::class	=> 1001,
		\App\Events\Admin\WithDrawProvider::class	=> 1002,

		/**
		 * Provider
		 */
		\App\Events\Provider\AddNewOffice::class	=> 2001,
		\App\Events\Provider\EditOffice::class		=> 2002,
		\App\Events\Provider\AddNewService::class	=> 2003,
		\App\Events\Provider\EditService::class		=> 2004,
		\App\Events\Provider\AddNewEmployee::class	=> 2005,
		\App\Events\Provider\EditEmployee::class	=> 2006,
		\App\Events\Provider\AddNewSchedule::class	=> 2007,


		/**
		 * Web
		 */
		\App\Events\Web\ConvertRequest::class		=> 3001,
		\App\Events\Web\UserRegister::class			=> 3002,
	];

	// ---------------- relations ---------------- //

	public function user()
	{
		return $this->hasOne(User::class);
	}

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //

	public static function getActionId(string $string)
	{
		if (!isset(self::$actions[$string])) {
			throw new \Exception("$string Not define in " . self::class);
		}
		return self::$actions[$string];
	}
}
