<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	use HasFactory;

	protected $fillable = [
		"owner",
		"user_id",
		"label",
	];

	// ---------------- relations ---------------- //
	public function user()
	{
		return $this->hasOne(User::class, "id", "user_id");
	}

	public function owner()
	{
		return $this->hasOne(User::class, "id", "owner");
	}

	public function services($office_id = null)
	{
		$services = $this->belongsToMany(Service::class, "employee_permission");

		return $office_id ? $services->where("office_id", $office_id)->get() : $services;
	}

	public function offices()
	{
		return $this->belongsToMany(Office::class, "employee_permission");
	}

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //

}
