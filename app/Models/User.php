<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use App\Services\Permission\Traits\HasRoles;
use App\Services\Permission\Traits\HasPermissions;
use App\Services\Scheduling\Schedulable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use HasFactory, Notifiable, HasPermissions, HasRoles, Schedulable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	protected $fillable = [
		"mobile_number",
		"national_code",
		"email",
		"first_name",
		"last_name",
		"birth_day",
		"city",
		"phone_number",
		"zip_code",
		"address",
		"wallet"
	];

	// ---------------- relations ---------------- //

	public function ProviderRequest()
	{
		return $this->hasOne(ProviderRequest::class);
	}

	public function offices()
	{
		return $this->hasMany(Office::class);
	}

	public function logs()
	{
		return $this->hasMany(Log::class);
	}

	public function services()
	{
		return $this->hasMany(Service::class);
	}

	public function employees()
	{
		return $this->hasMany(Employee::class, "owner");
	}

	// ---------------- instance methods ---------------- //

	public function convertToProviderRequest(): void
	{
		$this->ProviderRequest()->exists()
			? $this->ProviderRequest()->touch()
			: $this->ProviderRequest()->create();
	}

	public function isUserOnPending(): bool
	{
		return $this->ProviderRequest()->exists();
	}

	public function is_provider(): bool
	{
		return $this->hasRole("provider");
	}

	// ---------------- static methods ---------------- //

	public static function loginMobileNumber($mobile_number): void
	{
		Auth::login(
			User::firstOrCreate(
				[
					"mobile_number" => $mobile_number
				]
			)
		);
	}
}
