<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    // ---------------- relations ---------------- //

    public function orders()
    {
        $this->belongsToMany(Order::class, "order_product");
    }

    // ---------------- instance methods ---------------- //

    // ---------------- static methods ---------------- //
}
