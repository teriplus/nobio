<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	use HasFactory;

	// ---------------- relations ---------------- //

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //

	public static function name($city_id)
	{
		$cities = self::getCities();
		return $cities[$city_id] ?? "unknown";
	}

	private static function getCities(): array
	{
		return [
			"1" => "تهران",
			"2" => "کرج",
			"3" => "مشهد",
			"4" => "اصفهان",
		];
	}
}
