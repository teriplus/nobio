<?php

namespace App\Models;

use App\Models\Office;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OfficeNumber extends Model
{
	use HasFactory;

	public const MOBILE_TYPE	= 1;
	public const PHONE_TYPE		= 2;

	protected $fillable = ["office_id", "number", "type"];

	public $timestamps = false;

	// ---------------- relations ---------------- //

	public function office()
	{
		return $this->hasOne(Office::class);
	}

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //
}
