<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	use HasFactory;

	// ---------------- relations ---------------- //

	public function payment()
	{
		return $this->hasOne(Payment::class);
	}

	public function products()
	{
		return $this->belongsToMany(products::class, "order_product");
	}

	// ---------------- instance methods ---------------- //

	// ---------------- static methods ---------------- //
}
