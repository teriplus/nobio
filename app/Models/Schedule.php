<?php

namespace App\Models;

use App\Models\Service;
use Morilog\Jalali\Jalalian;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Schedule extends Model
{
	use HasFactory;

	protected $fillable = [
		"day",
		"start_at",
		"end_at",
	];
	// ---------------- relations ---------------- //

	public function schedulable()
	{
		return $this->morphTo();
	}

	public function services()
    {
        return $this->belongsToMany(Service::class, "schedule_service");
    }
	
	// ---------------- instance methods ---------------- //

	public function hasOverlap($start_at, $end_at): bool
	{
		if ($end_at <= $this->start_at) {
			return false;
		} else if ($this->end_at <= $start_at) {
			return false;
		} else {
			return true;
		}
	}

	public function getTime($state, $separator = null)
	{
		$attr = $state . "_at";
		$minutes	= $this->$attr;

		$minute		= sprintf("%02d", $minutes % 60);
		$hour		= sprintf("%02d", ($minutes - $minute) / 60);

		if ($separator) {
			return $hour . $separator . $minute;
		}
		
		return [$hour, $minute];
	}
	// ---------------- static methods ---------------- //
}
