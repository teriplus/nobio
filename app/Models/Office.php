<?php

namespace App\Models;

use App\Models\City;
use App\Models\User;
use App\Models\Service;
use App\Models\Employee;
use App\Models\OfficeNumber;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Office extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "description",
        "address",
        "city",
        "instagram",
        "telegram"
    ];

    // ---------------- relations ---------------- //

    public function city()
    {
        return City::name($this->city);
    }

    public function numbers()
    {
        return $this->hasMany(OfficeNumber::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, "employee_permission");
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, "office_service");
    }

    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'schedulable');
    }

    // ---------------- instance methods ---------------- //

    // ---------------- static methods ---------------- //
}
