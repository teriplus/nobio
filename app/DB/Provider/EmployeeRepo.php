<?php

namespace App\DB\Provider;

use App\DB\Abortable;
use App\Models\Employee;

class EmployeeRepo
{
    use Abortable;

    private function getMyEmployee($auth_id, $employee_id)
    {
        return Employee
            ::where("id", $employee_id)
            ->where("owner", $auth_id)
            ->with("user:id,mobile_number")
            ->first();
    }

    private function getMyEmployees($auth_id)
    {
        return Employee
            ::where("owner", $auth_id)
            ->latest()
            ->get();
    }

    private function searchOnMyEmployees($auth_id, $search)
    {
        return Employee
            ::where("owner", $auth_id)
            ->with("user:id,mobile_number")
            ->where("label", "LIKE", "%" . $search . "%")
            // ->orWhereHas("user", function ($user) use ($search) {
            //     $user->where("mobile_number", "LIKE", "%" . $search . "%");
            // })
            ->latest()
            ->paginate(10);
    }
}
