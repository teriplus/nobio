<?php

namespace App\DB\Provider;

use App\DB\Abortable;
use App\Models\Service;

class ServiceRepo
{
	use Abortable;

	private function getMyService($auth_id, $service_id)
	{
		return Service
			::where("user_id", $auth_id)
			->find($service_id);
	}

	private function getMyServices($auth_id, array $get = ['*'])
	{
		return Service
			::where("user_id", $auth_id)
			->latest()
			->get($get);
	}

	private function searchOnMyServices($auth_id, $search)
	{
		return Service
			::where("user_id", $auth_id)
			->where("name", "LIKE", "%" . $search . "%")
			->latest()
			->paginate(10);
	}
}
