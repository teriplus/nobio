<?php

namespace App\DB\Provider;

use App\DB\Abortable;
use App\Models\Office;

class ScheduleRepo
{
	use Abortable;
	// public static function searchOnMyServices($auth_id, $search)
	// {
	//     return Schedule
	//         ::where("user_id", $auth_id)
	//         ->where("name", "LIKE", "%" . $search . "%")
	//         ->latest()
	//         ->paginate(10);
	// }

	private function getSchedeules($schedulable, $daykey)
	{
		return $schedulable
			->schedules()
			->where("day", $daykey)
			->orderBy("start_at", "ASC")
			->get();
	}
}
