<?php

namespace App\DB\Provider;

use App\DB\Abortable;
use App\Models\Office;

class OfficeRepo
{
	use Abortable;

	private function getMyOffice($auth_id, $office_id)
	{
		return Office
			::where("user_id", $auth_id)
			->with('numbers')
			->find($office_id);
	}

	private function getMyOffices($auth_id, array $get = ['*'])
	{
		return Office
			::where("user_id", $auth_id)
			->with("services")
			->latest()
			->get($get);
	}

	private function searchOnMyOffices($auth_id, $search)
	{
		return Office
			::where("user_id", $auth_id)
			->where("name", "LIKE", "%" . $search . "%")
			->latest()
			->paginate(10);
	}
}
