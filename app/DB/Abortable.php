<?php

namespace App\DB;

trait Abortable
{
    public function __call($method, $arguments)
    {
        if (!method_exists($this, $method)) {
            throw new \Exception("$method Not Found!");
        }

        return call_user_func_array(array($this, $method), $arguments) ?? abort(403);
    }
}
