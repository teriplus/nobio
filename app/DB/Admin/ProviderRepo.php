<?php

namespace App\DB\Admin;

use App\DB\Abortable;
use App\Models\User;
use App\Models\ProviderRequest;

class ProviderRepo
{
	use Abortable;

	private function getConvertList()
	{
		return ProviderRequest
			::with('user:id,first_name,last_name,mobile_number')
			->orderBy('updated_at', 'ASC')
			->get();
	}

	private function getConvertRequest($id)
	{
		return ProviderRequest
			::whereId($id)
			->with('user')
			->first();
	}

	private function getActiveList()
	{
		return User
			::whereHas("roles", function ($role) {
				$role->where("name", "provider");
			})
			->latest()
			->get();
	}

	private function getRejectList()
	{
		return ProviderRequest
			::whereNull("accepted_by")
			->whereNotNull("rejected_by")
			->with('user:id,first_name,last_name,mobile_number')
			->orderBy('updated_at', 'ASC')
			->get();
	}

	private function searchOnConvertList($search)
	{
		return ProviderRequest
			::with('user')
			->whereHas('user', function ($user) use ($search) {
				$user->where("first_name", "LIKE", "%" . $search . "%")
					->orWhere("last_name", "LIKE", "%" . $search . "%")
					->orWhere("mobile_number", "LIKE", "%" . $search . "%");
			})
			->orderBy('updated_at', 'ASC')
			->get();
	}

	private function searchOnActiveList($search)
	{
		return User
			::where("first_name", "LIKE", "%" . $search . "%")
			->orWhere("last_name", "LIKE", "%" . $search . "%")
			->orWhere("mobile_number", "LIKE", "%" . $search . "%")
			->whereHas("roles", function ($role) {
				$role->where("name", "provider");
			})
			->latest()
			->get();
	}
}
