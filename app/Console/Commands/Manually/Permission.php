<?php

namespace App\Console\Commands\Manually;

use App\Models\Permission as ModelsPermission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class Permission extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'make:permission {--id=1000} {--mn=} {--to=admin}';
	// php artisan make:permission --id=1 --to=admin
	// php artisan make:permission --mn=09194095098 --to=admin

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'make basic permission';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		/**
		 * Truncate Permissions
		 * Create Permissions
		 */
		self::permissions();


		/**
		 * Truncate Roles
		 * Create Roles
		 */
		self::roles();

		/**
		 * Assign Roles And Permissions
		 */
		self::role_permission();
	}

	private static function roles(): void
	{
		foreach (self::$roles as $role) {
			$is_exist = Role::where("name", $role["name"])->exists();
			if (!$is_exist) {
				Role::insert($role);
			}
		}
	}

	private static function permissions(): void
	{
		foreach (self::$permissions as $permission) {
			$is_exist = ModelsPermission::where("name", $permission["name"])->exists();
			if (!$is_exist) {
				ModelsPermission::insert($permission);
			}
		}
	}

	private static function role_permission(): void
	{
		foreach (self::$role_permission as $role => $permissions) {
			$role = Role::where("name", $role)->first();
			$role->givePermissionsTo($permissions);
		}
	}

	private static $roles = [
		[
			"name"			=> "admin",
			"persian_name"	=> "مدیر کل",
		],
		[
			"name"			=> "provider",
			"persian_name"	=> "ارائه دهنده",
		],
		[
			"name"			=> "employee",
			"persian_name"	=> "کارمند",
		],
	];

	private static $permissions = [
		[
			"name"			=> "provider.managment",
			"persian_name"	=> "مدیریت مشتریان",
		],
		[
			"name"			=> "provider.convert",
			"persian_name"	=> "ارتقا به ارائه دهندگان",
		],
		[
			"name"			=> "provider.active",
			"persian_name"	=> "مشتریان فعال",
		],
	];

	private static $role_permission = [
		"admin"		=> [
			"provider.managment",
			"provider.convert",
			"provider.active",
			"provider.reject"
		],
		"provider"	=> [],
		"employee"	=> [],
	];
	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		$roles	= explode(",", $this->option('to'));

		if ($this->option('mn')) {
			$user	= User::where("mobile_number", $this->option('mn'))->firstOrFail()->giveRolesTo($roles);
		} else {
			$user	= User::findOrFail($this->option('id'))->giveRolesTo($roles);
		}

		echo "user_id: $user->id,\t successfully upgraded! \n";
	}
}
