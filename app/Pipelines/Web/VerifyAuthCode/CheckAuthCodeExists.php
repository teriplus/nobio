<?php

namespace App\Pipelines\Web\VerifyAuthCode;

use Closure;
use App\Models\AuthCode;
use App\Pipelines\Pipeable;

class CheckAuthCodeExists implements Pipeable
{
    public function handle($content, Closure $next)
    {
        try {
            $AuthCode = AuthCode
                ::where("mobile_number", $content["this"]->mobile_number)
                ->firstOrFail();
        } catch (\Throwable $th) {
            return $content["this"]->addError('auth_code', "لطفا مجددا برای ارسال کد تایید اقدام کنید.");
        }

        return  $next(
            array_merge($content, [
                "authCode" => $AuthCode
            ])
        );
    }
}
