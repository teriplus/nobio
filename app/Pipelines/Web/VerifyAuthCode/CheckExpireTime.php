<?php

namespace App\Pipelines\Web\VerifyAuthCode;

use Closure;
use App\Pipelines\Pipeable;

class CheckExpireTime implements Pipeable
{
    public function handle($content, Closure $next)
    {

        if ($content["authCode"]->created_at < now()->subMinutes(5)) {
            return $content["this"]->addError('auth_code', "کد وارد شده منقضی شده است.");
        }

        return  $next($content);
    }
}
