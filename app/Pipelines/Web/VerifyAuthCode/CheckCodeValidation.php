<?php

namespace App\Pipelines\Web\VerifyAuthCode;

use Closure;
use App\Pipelines\Pipeable;

class CheckCodeValidation implements Pipeable
{
    public function handle($content, Closure $next)
    {

        if ($content["authCode"]->auth_code != $content["this"]->auth_code) {
            return $content["this"]->addError('auth_code', "کد وارد شده اشتباه است.");
        }

        return  $next($content);
    }
}
