<?php

namespace App\Pipelines\Provider\VerifyAddNewSchedule;

use Closure;
use App\Pipelines\Pipeable;

class CheckTimeValidation implements Pipeable
{
	public function handle($content, Closure $next)
	{
		[$content["this"]->start_hour, $content["this"]->start_min]	= explode(":", $content["this"]->start_time);
		[$content["this"]->end_hour, $content["this"]->end_min]		= explode(":", $content["this"]->end_time);
		[$content["this"]->today_hour, $content["this"]->today_min]	= explode(":", date("H:i"));

		$content["this"]->start_at	= $this->timeToTimeDay($content["this"]->start_hour, $content["this"]->start_min);
		$content["this"]->end_at	= $this->timeToTimeDay($content["this"]->end_hour, $content["this"]->end_min);
		$content["this"]->today_at	= $this->timeToTimeDay($content["this"]->today_hour, $content["this"]->today_min);

		if ($this->is_daykey_inـpast($content)) {
			session()->flash('message', [
				"danger"	=> "امکان ثبت برنامه فقط برای روزهای آینده امکان پذیر است."
			]);

			return;
		}

		if ($content["this"]->start_at > $content["this"]->end_at) {
			return $content["this"]->addError("end_time", "زمان پایان نمی‌تواند کوچک تر از زمان شروع باشد.");
		}

		if ($content["this"]->start_at == $content["this"]->end_at) {
			return $content["this"]->addError("end_time", "زمان شروع و زمان پایان نمی‌تواند یکی باشد!");
		}

		if ($this->is_daykey_inـfuture($content)) {
			return $next($content);
		}

		if ($content["this"]->today_at > $content["this"]->start_at) {
			return $content["this"]->addError("start_time", "زمان شروع نمی‌تواند گذشته باشد.");
		}

		return $next($content);
	}

	private function is_daykey_inـpast($content): bool
	{
		$schedule_daykey	= $content["this"]->scheduling["daykey"];
		$today_daykey		= jdate()->now()->format("Ymd");

		if ($schedule_daykey < $today_daykey) {
			return true;
		}

		return false;
	}

	private function is_daykey_inـfuture($content): bool
	{
		$schedule_daykey	= $content["this"]->scheduling["daykey"];
		$today_daykey		= jdate()->now()->format("Ymd");

		if ($schedule_daykey > $today_daykey) {
			return true;
		}

		return false;
	}

	private function timeToTimeDay($hour, $min): int
	{
		return  $hour * 60 + $min;
	}
}
