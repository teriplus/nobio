<?php

namespace App\Pipelines\Provider\VerifyAddNewSchedule;

use Closure;
use App\Pipelines\Pipeable;
use App\Services\Helper;

class CheckTimeFormat implements Pipeable
{
	public function handle($content, Closure $next)
	{
		$content["this"]->start_time	= Helper::intToEnglish($content["this"]->start_time);
		$content["this"]->end_time		= Helper::intToEnglish($content["this"]->end_time);

		$pattern = "/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/";

		if (!preg_match($pattern, $content["this"]->start_time)) {
			return $content["this"]->addError("start_time", "زمان وارد شده فرمت مناسبی ندارد.");
		}

		if (!preg_match($pattern, $content["this"]->end_time)) {
			return $content["this"]->addError("end_time", "زمان وارد شده فرمت مناسبی ندارد.");
		}

		return $next($content);
	}

	// ---------------- private methods ---------------- //
}
