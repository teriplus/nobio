<?php

namespace App\Pipelines\Provider\VerifyAddNewSchedule;

use Closure;
use App\Pipelines\Pipeable;

class CheckEmployees implements Pipeable
{
	public function handle($content, Closure $next)
	{
		$employees = [];
		foreach ($content["this"]->employee_items as $employee) {
			if ($employee["value"]) {
				$employees[] = $employee["id"];
			}
		}

		if (count($employees) < 1) {
			return $content["this"]->addError('employee_items', "انتخاب حداقل یک کارمند الزامی است.");
		}

		$content["this"]->selected_employees = $employees;

		return $next($content);
	}
}
