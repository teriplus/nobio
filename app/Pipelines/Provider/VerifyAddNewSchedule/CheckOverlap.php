<?php

namespace App\Pipelines\Provider\VerifyAddNewSchedule;

use Closure;
use App\Pipelines\Pipeable;
use App\DB\Provider\ScheduleRepo;

class CheckOverlap implements Pipeable
{
	public function handle($content, Closure $next)
	{
		$schedules = resolve(ScheduleRepo::class)
			->getSchedeules($content["this"]->office, $content["this"]->scheduling["daykey"]);

		foreach ($schedules as $schedule) {
			$has_overlap = $schedule->hasOverlap($content["this"]->start_at, $content["this"]->end_at);
			if ($has_overlap) {
				$id = $schedule->id;
				session()->flash('message', [
					"danger"	=> "این زمان بندی با برنامه زمانی شما با شناسه‌ی $id تداخل دارد."
				]);

				return;
			}
		}

		return $next($content);
	}
}
