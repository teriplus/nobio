<?php

namespace App\Pipelines\Provider\VerifyAddNewEmployee;

use Closure;
use App\Models\User;
use App\Models\Employee;
use App\Pipelines\Pipeable;

class CheckDuplicateEmployee implements Pipeable
{
	public function handle($content, Closure $next)
	{
		$user = $this->getUser($content["this"]->mobile_number);

		if (!$user) {
			return $next($content);
		}

		if ($this->checkEmployeeExsits($user)) {
			return $content["this"]->addError("mobile_number", "این شماره موبایل قبلا به عنوان کارمند شما ثبت شده است.");
		}

		return $next($content);
	}

	// ---------------- private methods ---------------- //

	private function getUser($mobile_number)
	{
		return User::where("mobile_number", $mobile_number)->first();
	}

	private function checkEmployeeExsits(User $user): bool
	{
		return Employee::where("user_id", $user->id)->where("owner", auth()->user()->id)->exists();
	}
}
