<?php

namespace App\Pipelines\Provider\VerifyAddNewEmployee;

use Closure;
use App\Pipelines\Pipeable;

class CheckEmployeeItems implements Pipeable
{
	public function handle($content, Closure $next)
	{
		$offices	= $content["this"]->items;
		$selected	= [];

		foreach ($offices as $office) {

			$has_service = false;
			$has_office = false;

			if ($office["value"] == true) {
				$has_office = true;
				foreach ($office["services"] as $service) {
					if ($service["value"] == true) {
						$has_service = true;
						$selected[$office["id"]][] = $service["id"];
					}
				}
			}

			if ($has_office && !$has_service) {
				return $content["this"]->addError("service", "کارمند نمی‌تواند در دفتری مشغول باشد اما مسئولیتی نداشته باشد.");
			}
		}

		if (!$selected) {
			return $content["this"]->addError("office", "کارمند باید حداقل در یک دفتر یک خدمت را ارائه دهد.");
		}

		$content["this"]->selected = $selected;
		
		return $next($content);
	}
}
