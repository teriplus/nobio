<?php

namespace App\Pipelines\Provider\VerifyAddNewOffice;

use Closure;
use App\Pipelines\Pipeable;

class CheckServices implements Pipeable
{
	public function handle($content, Closure $next)
	{
		$services = [];
		foreach ($content["this"]->service_items as $service) {
			if ($service["value"]) {
				$services[] = $service["id"];
			}
		}

		if (count($services) < 1) {
			return $content["this"]->addError('service_items', "فیلد مهارت‌ها الزامی است.");
		}

		$content["this"]->selected_services = $services;

		return $next($content);
	}
}
