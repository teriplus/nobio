<?php

namespace App\Pipelines;

use Closure;

interface Pipeable
{
    public function handle($content, Closure $next);
}
