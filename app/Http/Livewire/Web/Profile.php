<?php

namespace App\Http\Livewire\Web;

use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Profile extends Component
{
	use AuthorizesRequests;

	public $first_name;
	public $last_name;
	public $email;
	public $is_provider;

	protected function rules()
	{
		return [
			'first_name'	=> 'nullable|string|min:3',
			'last_name'		=> 'nullable|string|min:3',
			'email'			=> "nullable|email|unique:users,email," . auth()->user()->id,
		];
	}

	public function mount()
	{
		$this->first_name		= auth()->user()->first_name;
		$this->last_name		= auth()->user()->last_name;
		$this->mobile_number	= auth()->user()->mobile_number;
		$this->email			= auth()->user()->email;
	}

	public function edit()
	{
		$this->validate();

		if (request()->user()->is_provider()) {
			session()->flash('message', [
				"warning"	=> "امکان ویرایش اطلاعات برای ارائه دهندگان امکان ندارد. درصورت نیاز تیکت بزنید."
			]);
			return back();
		}

		request()->user()->update([
			"first_name"	=> $this->first_name,
			"last_name"		=> $this->last_name,
			"email"			=> $this->email,
		]);

		session()->flash('message', [
			"success"	=> "پروفایل شما باموفقیت بروزرسانی شد."
		]);
	}

	public function render()
	{
		return view('livewire.web.profile');
	}
}
