<?php

namespace App\Http\Livewire\Web;

use Livewire\Component;
use App\Models\ProviderRequest;
use App\Services\Helper;
use App\Events\Web\ConvertRequest;

class Convert extends Component
{
	public $first_name;
	public $last_name;
	public $national_code;
	public $email;
	public $city;
	public $birth_day;
	public $phone_number;
	public $mobile_number;
	public $zip_code;
	public $address;

	public $isUserRequested;

	protected function rules()
	{
		return [
			"first_name"	=> "required|string|min:2",
			"last_name"		=> "required|string|min:2",
			"national_code"	=> "required|regex:/(^.{10}$)/u|unique:users,national_code," . auth()->user()->id,
			"email"			=> "required|email|unique:users,email," . auth()->user()->id,
			"city"			=> "required",
			"phone_number"	=> "required|regex:/(^.{8}$)/u|unique:users,phone_number," . auth()->user()->id,
			"zip_code"		=> "required|regex:/(^.{10}$)/u|unique:users,zip_code," . auth()->user()->id,
			"address"		=> "required|string|max:500",
		];
	}

	public function booted()
	{
		if (request()->user()->hasRole("provider")) {
			redirect()->route("provider.dashboard");
		}
	}

	public function mount()
	{
		if (ProviderRequest::hasUserActiveRequest(request()->user())) {
			session()->flash('message', [
				"warning"	=> "درخواست شما پیش ازین ارسال شده است و درحال برسی میباشد، درصورت ثبت مجدد زمان پاسخ دهی افزایش میابد."
			]);
		}

		$this->first_name		= auth()->user()->first_name;
		$this->last_name		= auth()->user()->last_name;
		$this->national_code	= auth()->user()->national_code;
		$this->email			= auth()->user()->email;
		$this->city				= auth()->user()->city;
		$this->phone_number		= auth()->user()->phone_number;
		$this->mobile_number	= auth()->user()->mobile_number;
		$this->zip_code			= auth()->user()->zip_code;
		$this->address			= auth()->user()->address;
	}

	public function convert()
	{
		$user_info = $this->validate();

		ConvertRequest::dispatch($user_info);

		session()->flash('message', [
			"success"	=> "درخواست باموفقیت ارسال شد."
		]);
	}

	public function render()
	{
		return view('livewire.web.convert');
	}
}
