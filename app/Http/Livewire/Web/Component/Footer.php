<?php

namespace App\Http\Livewire\Web\Component;

use Livewire\Component;

class Footer extends Component
{
    public function render()
    {
        return view('livewire.web.component.footer');
    }
}
