<?php

namespace App\Http\Livewire\Web\Component;

use Livewire\Component;

class Scrolltop extends Component
{
    public function render()
    {
        return view('livewire.web.component.scrolltop');
    }
}
