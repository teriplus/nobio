<?php

namespace App\Http\Livewire\Web\Component;

use Livewire\Component;

class MobileHeader extends Component
{
    public function render()
    {
        return view('livewire.web.component.mobile-header');
    }
}
