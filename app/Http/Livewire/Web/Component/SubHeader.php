<?php

namespace App\Http\Livewire\Web\Component;

use Livewire\Component;

class SubHeader extends Component
{
    public $title;

    // public function mount($title)
    // {
    //     $this->title = $title;
    // }

    public function render()
    {
        return view('livewire.web.component.sub-header');
    }
}
