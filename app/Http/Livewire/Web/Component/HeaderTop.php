<?php

namespace App\Http\Livewire\Web\Component;

use Livewire\Component;

class HeaderTop extends Component
{
    public function render()
    {
        return view('livewire.web.component.header-top');
    }
}
