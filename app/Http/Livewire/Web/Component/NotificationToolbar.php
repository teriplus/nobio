<?php

namespace App\Http\Livewire\Web\Component;

use Livewire\Component;

class NotificationToolbar extends Component
{
    public function render()
    {
        return view('livewire.web.component.notification-toolbar');
    }
}
