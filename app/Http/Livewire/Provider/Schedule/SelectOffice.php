<?php

namespace App\Http\Livewire\Provider\Schedule;

use Livewire\Component;
use App\DB\Provider\OfficeRepo;
use App\Models\City;

class SelectOffice extends Component
{
	private $items;
	private OfficeRepo $officeRepo;
	public $search;
	public $auth_id;

	public function boot()
	{
		$this->auth_id		= auth()->user()->id;
		$this->officeRepo	= resolve(OfficeRepo::class);

		$this->items = $this->officeRepo->getMyOffices($this->auth_id);
	}

	public function search()
	{
		$this->items = $this->officeRepo->searchOnMyOffices($this->auth_id, $this->search);
	}

	public function redirectToOfficeSchedules($office_id)
	{
		return redirect()->route("provider.schedule.office-schedule", [$office_id]);
	}

	public function render()
	{
		return view('livewire.provider.schedule.select-office', [
			"items" => $this->items
		])->layout('layouts.provider');
	}
}
