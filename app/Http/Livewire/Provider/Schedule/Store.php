<?php

namespace App\Http\Livewire\Provider\Schedule;

use Livewire\Component;

class Store extends Component
{
    public function render()
    {
        return view('livewire.provider.schedule.store')
            ->layout('layouts.provider');
    }
}
