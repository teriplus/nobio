<?php

namespace App\Http\Livewire\Provider\Schedule;

use Livewire\Component;

class ScheduleList extends Component
{
    public function render()
    {
        return view('livewire.provider.schedule.schedule-list')
            ->layout('layouts.provider');
    }
}
