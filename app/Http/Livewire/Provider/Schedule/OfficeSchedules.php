<?php

namespace App\Http\Livewire\Provider\Schedule;

use App\Models\Office;
use Livewire\Component;
use App\DB\Provider\OfficeRepo;
use App\DB\Provider\ScheduleRepo;
use Illuminate\Pipeline\Pipeline;
use App\Events\Provider\AddNewSchedule;

class OfficeSchedules extends Component
{
	private OfficeRepo $officeRepo;
	private ScheduleRepo $scheduleRepo;
	private $auth_id;

	public $week_limit = 4;

	public Office $office;
	public $employee_items = [];
	public $start_time;
	public $end_time;

	public $schedulable_type = "hidden";
	public $scheduling = [];

	protected function rules()
	{
		return [
			"employee_items"	=> "required|array",
			"start_time"		=> "required|string",
			"end_time"			=> "required|string",
		];
	}

	public function boot()
	{
		$this->auth_id		= auth()->user()->id;
		$this->officeRepo	= resolve(OfficeRepo::class);
		$this->scheduleRepo	= resolve(ScheduleRepo::class);
	}

	public function mount($office_id)
	{
		$this->office		= $this->officeRepo->getMyOffice($this->auth_id, $office_id);

		$this->employee_items	= $this->office->employees->unique()->map(function ($employee_item) {
			return [
				"id"		=> $employee_item->id,
				"label"		=> $employee_item->label,
				"value"		=> true
			];
		});
	}

	public function is_schedulable(array $day)
	{
		$this->scheduling	= $day;

		if (true) {
			$this->possibleToScheduling();
		} else {
			$this->impossibleToScheduling();
		}
	}

	private function possibleToScheduling()
	{
		$this->reset("start_time", "end_time");

		$this->schedulable_type	= "possible";
		$this->schedule_history	= $this->scheduleRepo->getSchedeules($this->office, $this->scheduling["daykey"]);
	}

	private function impossibleToScheduling()
	{
		$this->schedulable_type	= "impossible";
	}

	public function hideScheduling()
	{
		$this->schedulable_type	= "hidden";
	}

	public function increaseWeekLimit()
	{
		$this->week_limit += 1;
	}

	public function add()
	{
		$this->validate();

		$result = app(Pipeline::class)
			->send(["this"	=> $this])
			->through([
				\App\Pipelines\Provider\VerifyAddNewSchedule\CheckTimeFormat::class,
				\App\Pipelines\Provider\VerifyAddNewSchedule\CheckTimeValidation::class,
				\App\Pipelines\Provider\VerifyAddNewSchedule\CheckOverlap::class,
				\App\Pipelines\Provider\VerifyAddNewSchedule\CheckEmployees::class,
			])->then(function ($content) {
				return $this->addNewSchedule($content);
			});

		return $result;
	}

	private function addNewSchedule($content)
	{
		AddNewSchedule::dispatch($this->office, $content["this"]);

		session()->flash('message', [
			"success"	=> "برنامه زمانی جدید باموفقیت افزوده شد."
		]);

		$this->reset("start_time", "end_time");
		$this->schedule_history	= $this->scheduleRepo->getSchedeules($this->office, $this->scheduling["daykey"]);
	}

	public function render()
	{
		return view('livewire.provider.schedule.office-schedules', [
			"weeks" => request()->user()->getNextWeeks($this->week_limit)
		])->layout('layouts.provider');
	}
}
