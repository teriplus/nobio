<?php

namespace App\Http\Livewire\Provider\Schedule;

use Livewire\Component;

class Edit extends Component
{
    public function render()
    {
        return view('livewire.provider.schedule.edit')
            ->layout('layouts.provider');
    }
}
