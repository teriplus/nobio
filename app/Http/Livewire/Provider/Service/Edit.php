<?php

namespace App\Http\Livewire\Provider\Service;

use App\Models\Service;
use Livewire\Component;
use App\Services\Helper;
use App\DB\Provider\ServiceRepo;
use App\Events\Provider\EditService;

class Edit extends Component
{
	private ServiceRepo $serviceRepo;
	private int $auth_id;

	public Service $service;

	public $name;
	public $description;
	public $time;

	public function rules()
	{
		return [
			"name"			=> "required|min:3|max:255",
			"description"	=> "nullable",
			"time"			=> "required|numeric",
		];
	}

	public function boot()
	{
		$this->serviceRepo = resolve(ServiceRepo::class);
		$this->auth_id = auth()->user()->id;
	}

	public function mount($service_id)
	{
		$this->service		= $this->serviceRepo->getMyService($this->auth_id, $service_id);

		$this->name			= $this->service->name;
		$this->description	= $this->service->description;
		$this->time			= $this->service->time;
	}

	public function edit()
	{
		$service_info = $this->validate();

		EditService::dispatch($this->service, $service_info);

		session()->flash('message', [
			"success"	=> "خدمت باموفقیت ویرایش شد."
		]);

		return redirect()->route("provider.service.list");
	}

	public function render()
	{
		return view('livewire.provider.service.edit')
			->layout('layouts.provider');
	}
}
