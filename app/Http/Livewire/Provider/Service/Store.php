<?php

namespace App\Http\Livewire\Provider\Service;

use Livewire\Component;
use App\Services\Helper;
use App\Events\Provider\AddNewService;

class Store extends Component
{
	public $name;
	public $description;
	public $time;

	public function rules()
	{
		return [
			"name"			=> "required|min:3|max:255",
			"description"	=> "nullable",
			"time"			=> "required|numeric|min:5|max:200",
		];
	}

	public function add()
	{
		$service_info = $this->validate();

		AddNewService::dispatch($service_info);

		session()->flash('message', [
			"success"	=> "خدمت جدید اضافه شد."
		]);

		return redirect()->route("provider.service.list");
	}

	public function render()
	{
		return view('livewire.provider.service.store')
			->layout('layouts.provider');
	}
}
