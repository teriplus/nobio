<?php

namespace App\Http\Livewire\Provider\Service;

use Livewire\Component;
use App\DB\Provider\ServiceRepo;

class ServiceList extends Component
{
    private ServiceRepo $serviceRepo;
    private $items;
    private $auth_id;

    public $search;

    public function boot()
    {
        $this->auth_id      = auth()->user()->id;
        $this->serviceRepo  = resolve(ServiceRepo::class);
        $this->items        = $this->serviceRepo->getMyServices($this->auth_id);
    }

    public function search()
    {
        $this->items = $this->serviceRepo->searchOnMyServices($this->auth_id, $this->search);
    }

    public function render()
    {
        return view('livewire.provider.service.service-list', [
            "items" => $this->items
        ])->layout('layouts.provider');
    }
}
