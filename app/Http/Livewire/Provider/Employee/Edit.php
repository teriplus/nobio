<?php

namespace App\Http\Livewire\Provider\Employee;

use Livewire\Component;
use App\Models\Employee;
use App\DB\Provider\OfficeRepo;
use App\DB\Provider\ServiceRepo;
use App\DB\Provider\EmployeeRepo;
use Illuminate\Pipeline\Pipeline;
use App\Events\Provider\EditEmployee;

class Edit extends Component
{
	private $auth_id;
	private EmployeeRepo $employeeRepo;
	private ServiceRepo $serviceRepo;
	private OfficeRepo $officeRepo;

	public Employee $employee;
	public $items	= [];
	public $label;
	public $mobile_number;

	protected function rules()
	{
		return [
			"label"			=> "required|min:3|max:255",
			"mobile_number"	=> "required|regex:/(^09[0-9]{9}$)/u",
			"items"			=> "required|array",
		];
	}

	public function boot()
	{
		$this->auth_id = auth()->user()->id;
		$this->serviceRepo = resolve(ServiceRepo::class);
		$this->officeRepo = resolve(OfficeRepo::class);
		$this->employeeRepo = resolve(EmployeeRepo::class);
	}

	public function mount($employee_id)
	{
		$this->employee			= $this->employeeRepo->getMyEmployee($this->auth_id, $employee_id);
		$this->label			= $this->employee->label;
		$this->mobile_number	= $this->employee->user->mobile_number;

		$offices	= $this->officeRepo->getMyOffices($this->auth_id);

		foreach ($offices as $office) {
			$service_items = [];
			foreach ($office->services as $service) {
				$service_items[] = [
					"id"		=> $service->id,
					"name"		=> $service->name,
					"value"		=> $this->employee->services($office->id)->contains('id', $service->id),
				];
			}

			$this->items[] = [
				"id"		=> $office->id,
				"name"		=> $office->name,
				"value"		=> $this->employee->offices->contains('id', $office->id),
				"services"	=> $service_items
			];
		}
	}

	public function edit()
	{
		$result = app(Pipeline::class)
			->send(["this"	=> $this])
			->through([
				\App\Pipelines\Provider\VerifyAddNewEmployee\CheckEmployeeItems::class,
			])->then(function ($content) {
				return $this->editEmployee($content);
			});
		return $result;
	}

	public function render()
	{
		return view('livewire.provider.employee.edit')
			->layout('layouts.provider');
	}

	// ---------------- private methods ---------------- //

	private function editEmployee($content)
	{
		EditEmployee::dispatch($this->employee, $content["this"]);

		session()->flash('message', [
			"success"	=> "کارمند ویرایش شد."
		]);

		return redirect()->route("provider.employee.list");
	}
}
