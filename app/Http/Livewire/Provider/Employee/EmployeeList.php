<?php

namespace App\Http\Livewire\Provider\Employee;

use Livewire\Component;
use App\DB\Provider\EmployeeRepo;

class EmployeeList extends Component
{
	private $items;
	private $auth_id;
	private EmployeeRepo $employeeRepo;

	public $search;

	public function boot()
	{
		$this->auth_id = auth()->user()->id;
		$this->employeeRepo = resolve(EmployeeRepo::class);
		$this->items = $this->employeeRepo->getMyEmployees($this->auth_id);
	}

	public function search()
	{
		$this->items = $this->employeeRepo->searchOnMyEmployees($this->auth_id, $this->search);
	}

	public function render()
	{
		return view('livewire.provider.employee.employee-list', [
			"items" => $this->items
		])->layout('layouts.provider');
	}
}
