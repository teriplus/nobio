<?php

namespace App\Http\Livewire\Provider\Employee;

use Livewire\Component;
use App\Services\Helper;
use App\DB\Provider\OfficeRepo;
use App\DB\Provider\ServiceRepo;
use Illuminate\Pipeline\Pipeline;
use App\Events\Provider\AddNewEmployee;

class Store extends Component
{
	private $auth_id;
	private ServiceRepo $serviceRepo;
	private OfficeRepo $officeRepo;

	public $items			= [];
	public $label;
	public $mobile_number;

	protected function rules()
	{
		return [
			"label"				=> "required|min:3|max:255",
			"mobile_number"		=> "required|regex:/(^09[0-9]{9}$)/u",
			"items"				=> "required|array",
		];
	}

	public function boot()
	{
		$this->auth_id		= auth()->user()->id;
		$this->serviceRepo	= resolve(ServiceRepo::class);
		$this->officeRepo	= resolve(OfficeRepo::class);
	}

	public function mount()
	{
		$services	= $this->serviceRepo->getMyServices($this->auth_id)->map(function ($service_item) {
			return [
				"id"		=> $service_item->id,
				"name"		=> $service_item->name,
				"value"		=> false
			];
		});

		$this->items		= $this->officeRepo->getMyOffices($this->auth_id)->map(function ($office_item) use ($services) {
			return [
				"id"		=> $office_item->id,
				"name"		=> $office_item->name,
				"value"		=> false,
				"services"	=> $services->toArray()
			];
		});
	}

	public function add()
	{
		$this->validate();

		$result = app(Pipeline::class)
			->send(["this"	=> $this])
			->through([
				\App\Pipelines\Provider\VerifyAddNewEmployee\CheckDuplicateEmployee::class,
				\App\Pipelines\Provider\VerifyAddNewEmployee\CheckEmployeeItems::class,
			])->then(function ($content) {
				return $this->addNewEmployee($content);
			});

		return $result;
	}

	public function render()
	{
		return view('livewire.provider.employee.store')
			->layout('layouts.provider');
	}

	// ---------------- private methods ---------------- //

	private function addNewEmployee($content)
	{
		AddNewEmployee::dispatch($content["this"]);

		session()->flash('message', [
			"success"	=> "کارمند جدید اضافه شد."
		]);

		return redirect()->route("provider.employee.list");
	}
}
