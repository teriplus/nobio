<?php

namespace App\Http\Livewire\Provider\Office;

use Livewire\Component;
use App\DB\Provider\ServiceRepo;
use Illuminate\Pipeline\Pipeline;
use App\Events\Provider\AddNewOffice;

class Store extends Component
{
	private ServiceRepo $serviceRepo;

	public $name;
	public $city;
	public $description;
	public $address;

	public $phone_numbers = [];
	public $mobile_numbers = [];

	public $phone_numbers_count = 1;
	public $mobile_numbers_count = 1;

	public $instagram;
	public $telegram;

	public $service_items	= [];

	protected function rules()
	{
		return [
			"name"			=> "required|min:3|max:255",
			"description"	=> "required|min:3|max:255",
			"address"		=> "required|min:3",
			"city"			=> "required",

			"phone_numbers"		=> "required|array|min:1",
			"mobile_numbers"	=> "required|array|min:1",

			"phone_numbers.*"	=> "required|regex:/(^\d+)/u",
			"mobile_numbers.*"	=> "required|regex:/(^09[0-9]{9}$)/u",

			"instagram"		=> "nullable|max:255",
			"telegram"		=> "nullable|max:255",

			"service_items"	=> "required|array",
		];
	}

	public function boot()
	{
		$this->auth_id		= auth()->user()->id;
		$this->serviceRepo	= resolve(ServiceRepo::class);
	}

	public function mount()
	{
		$this->service_items	= $this->serviceRepo->getMyServices($this->auth_id)->map(function ($service_item) {
			return [
				"id"		=> $service_item->id,
				"name"		=> $service_item->name,
				"value"		=> false
			];
		});
	}

	public function add()
	{
		$this->validate();

		$result = app(Pipeline::class)
			->send(["this"	=> $this])
			->through([
				\App\Pipelines\Provider\VerifyAddNewOffice\CheckServices::class,
			])->then(function ($content) {
				return $this->addNewOffice($content);
			});

		return $result;
	}

	public function addNewPhoneNumber()
	{
		$this->phone_numbers_count++;
	}

	public function addNewMobileNumber()
	{
		$this->mobile_numbers_count++;
	}

	private function addNewOffice($content)
	{
		AddNewOffice::dispatch($content["this"]);

		session()->flash('message', [
			"success"	=> "دفتر جدید باموفقیت افزوده شد."
		]);

		return redirect()->route("provider.office.list");
	}

	public function render()
	{
		return view('livewire.provider.office.store')
			->layout('layouts.provider');
	}
}
