<?php

namespace App\Http\Livewire\Provider\Office;

use App\DB\Provider\OfficeRepo;
use Livewire\Component;

class OfficeList extends Component
{
	private $items;
	private OfficeRepo $officeRepo;
	private $auth_id;

	public $search;

	public function boot()
	{
		$this->auth_id		= auth()->user()->id;
		$this->officeRepo	= resolve(OfficeRepo::class);

		$this->items = $this->officeRepo->getMyOffices($this->auth_id);
	}

	public function search()
	{
		$this->items = $this->officeRepo->searchOnMyOffices($this->auth_id, $this->search);
	}

	public function render()
	{
		return view('livewire.provider.office.office-list', [
			"items" => $this->items
		])->layout("layouts.provider");
	}
}
