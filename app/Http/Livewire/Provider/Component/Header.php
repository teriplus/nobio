<?php

namespace App\Http\Livewire\Provider\Component;

use Livewire\Component;

class Header extends Component
{
    public function render()
    {
        return view('livewire.provider.component.header');
    }
}
