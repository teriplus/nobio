<?php

namespace App\Http\Livewire\Provider\Component;

use Livewire\Component;

class MobileHeader extends Component
{
    public function render()
    {
        return view('livewire.provider.component.mobile-header');
    }
}
