<?php

namespace App\Http\Livewire\Provider\Component;

use Livewire\Component;

class Scrolltop extends Component
{
    public function render()
    {
        return view('livewire.provider.component.scrolltop');
    }
}
