<?php

namespace App\Http\Livewire\Component;

use Livewire\Component;

class Toast extends Component
{
    public $message;
    public $color;

    public function render()
    {
        return view('livewire.component.toast');
    }
}
