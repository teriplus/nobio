<?php

namespace App\Http\Livewire\Admin\Provider;

use App\Events\Admin\WithDrawProvider;
use Livewire\Component;

class WithDraw extends Component
{
    public function mount($id)
    {
        WithDrawProvider::dispatch($id);

        session()->flash('message', [
            "success"    => "دسترسی کاربر به ارايه دهنده حذف شد"
        ]);

        return redirect()
            ->route("admin.provider.active-list");
    }
}
