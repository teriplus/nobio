<?php

namespace App\Http\Livewire\Admin\Provider;

use Livewire\Component;
use App\Services\Helper;
use App\DB\Admin\ProviderRepo;
use App\Events\Admin\AcceptToProvider;
use App\Events\Admin\RejectToProvider;

class ConvertRecord extends Component
{
	private $provider_id;
	private ProviderRepo $providerRepo;

	public $request;
	public $user_id;
	public $first_name;
	public $last_name;
	public $national_code;
	public $email;
	public $city;
	public $birth_day;
	public $phone_number;
	public $mobile_number;
	public $zip_code;
	public $address;

	protected function rules()
	{
		return [
			"first_name"	=> "required|string|min:2",
			"last_name"		=> "required|string|min:2",
			"national_code"	=> "required|regex:/(^.{10}$)/u|unique:users,national_code," . $this->user_id,
			"email"			=> "required|email|unique:users,email," . $this->user_id,
			"city"			=> "required",
			"phone_number"	=> "required|regex:/(^.{8}$)/u|unique:users,phone_number," . $this->user_id,
			"zip_code"		=> "required|regex:/(^.{10}$)/u|unique:users,zip_code," . $this->user_id,
			"address"		=> "required|string|max:500",
		];
	}

	public function boot()
	{
		$this->providerRepo = resolve(ProviderRepo::class);
	}

	public function mount($id)
	{
		$this->provider_id		= $id;
		$this->request			= $this->providerRepo->getConvertRequest($this->provider_id) ?? abort(404);

		$this->user_id			= $this->request->user->id;
		$this->first_name		= $this->request->user->first_name;
		$this->last_name		= $this->request->user->last_name;
		$this->national_code	= $this->request->user->national_code;
		$this->email			= $this->request->user->email;
		$this->city				= $this->request->user->city;
		$this->phone_number		= $this->request->user->phone_number;
		$this->mobile_number	= $this->request->user->mobile_number;
		$this->zip_code			= $this->request->user->zip_code;
		$this->address			= $this->request->user->address;
	}

	public function accept()
	{
		$user_info = $this->validate();

		AcceptToProvider::dispatch($this->user_id, $user_info);

		session()->flash('message', [
			"success"	=> "کاربر تایید شد."
		]);

		return redirect()->route("admin.provider.convert-list");
	}

	public function reject()
	{
		RejectToProvider::dispatch($this->user_id);

		session()->flash('message', [
			"success"	=> "درخواست کاربر رد شد."
		]);

		return redirect()->route("admin.provider.convert-list");
	}

	public function render()
	{
		return view('livewire.admin.provider.convert-record')
			->layout('layouts.admin');
	}
}
