<?php

namespace App\Http\Livewire\Admin\Provider;

use App\DB\Admin\ProviderRepo;
use Livewire\Component;
use Livewire\WithPagination;

class ActiveList extends Component
{
    /**
     * Show Active Providers
     */

    use WithPagination;

    private $items;
	private ProviderRepo $providerRepo;

    public $search;

    public function boot()
    {
		$this->providerRepo = resolve(ProviderRepo::class);
        $this->items = $this->providerRepo->getActiveList();
    }

    public function search()
    {
        $this->items = $this->providerRepo->searchOnActiveList($this->search);
    }

    public function render()
    {
        return view('livewire.admin.provider.active-list', [
            "items"  => $this->items
        ])->layout('layouts.admin');
    }
}
