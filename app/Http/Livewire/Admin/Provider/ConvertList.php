<?php

namespace App\Http\Livewire\Admin\Provider;

use Livewire\Component;
use Livewire\WithPagination;
use App\DB\Admin\ProviderRepo;

class ConvertList extends Component
{
	/**
	 * Show Accepted/Rejected/New ConvertRequest's 
	 */
	use WithPagination;

	private ProviderRepo $providerRepo;
	private $items;

	public $search;

	public function boot()
	{
		$this->providerRepo = resolve(ProviderRepo::class);
		$this->items = $this->providerRepo->getConvertList();
	}

	public function search()
	{
		$this->items = $this->providerRepo->searchOnConvertList($this->search);
	}

	public function render()
	{
		return view('livewire.admin.provider.convert-list', [
			"items"  => $this->items
		])->layout('layouts.admin');
	}
}
