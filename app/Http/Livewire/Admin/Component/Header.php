<?php

namespace App\Http\Livewire\Admin\Component;

use Livewire\Component;

class Header extends Component
{
    public function render()
    {
        return view('livewire.admin.component.header');
    }
}
