<?php

namespace App\Http\Livewire\Admin\Component;

use Livewire\Component;

class Scrolltop extends Component
{
    public function render()
    {
        return view('livewire.admin.component.scrolltop');
    }
}
