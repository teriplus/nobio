<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;
use App\Models\AuthCode;
use App\Rules\mobileNumber;
use App\Events\Web\UserRegister;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Session;

class Register extends Component
{
	public $mobile_number;
	public $auth_code;
	public $submit = "register";

	protected function rules()
	{
		$rules = [
			'mobile_number'	=> 'required|regex:/(^09[0-9]{9}$)/u',
		];

		return $this->submit == "verify"
			? array_merge($rules, ['auth_code'	=> 'required|regex:/(^\d{4}$)/u'])
			: $rules;
	}

	public function register()
	{
		$this->validate();
		UserRegister::dispatch($this->mobile_number);

		$this->submit = "verify";
	}

	public function verify()
	{
		$this->validate();

		$result = app(Pipeline::class)
			->send(["this"	=> $this])
			->through([
				\App\Pipelines\Web\VerifyAuthCode\CheckAuthCodeExists::class,
				\App\Pipelines\Web\VerifyAuthCode\CheckExpireTime::class,
				\App\Pipelines\Web\VerifyAuthCode\CheckCodeValidation::class,
			])
			->then(function () {
				AuthCode::where("mobile_number", $this->mobile_number)->delete();
				User::loginMobileNumber($this->mobile_number);
				session()->flash('success', 'خوش‌آمدید!');
				redirect()->to(Session::get('back_url') ?? "/");
			});

		return $result;
	}

	public function edit()
	{
		$this->submit = "register";
	}

	public function render()
	{
		return view('livewire.auth.register', [
			"mobile_number" => $this->mobile_number
		]);
	}
}
