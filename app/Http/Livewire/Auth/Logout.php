<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Http\Request;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Logout extends Component
{
    public function mount(Request $request)
    {
        Auth::logout();

        session()->invalidate();
        session()->regenerateToken();
        session()->flash('success', 'با موفقیت خارج شدید.');

        return redirect()->route("web.home");
    }

    public function render()
    {
        return view('livewire.auth.logout');
    }
}
