<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array<class-string, array<int, class-string>>
	 */
	protected $listen = [
		/**
		 * Admin
		 */
		\App\Events\Admin\AcceptToProvider::class => [
			\App\Listeners\Admin\ConvertToProvider\UpdateUserInfo::class,
			\App\Listeners\Admin\ConvertToProvider\UpdateProviderRequest::class,
			\App\Listeners\Admin\ConvertToProvider\SendNotification::class,
			\App\Listeners\Admin\ConvertToProvider\GiveProviderRole::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Admin\RejectToProvider::class => [
			\App\Listeners\Admin\RejectToProvider\MoveUserToRejectList::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Admin\WithDrawProvider::class => [
			\App\Listeners\Admin\WithDrawProvider\WithDraw::class,
			\App\Listeners\LogMe::class
		],

		/**
		 * Provider
		 */
		\App\Events\Provider\AddNewOffice::class => [
			\App\Listeners\Provider\AddNewOffice\InsertOffice::class,
			\App\Listeners\Provider\AddNewOffice\InsertNumbers::class,
			\App\Listeners\Provider\AddNewOffice\SyncServices::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Provider\EditOffice::class => [
			\App\Listeners\Provider\EditOffice\EditOffice::class,
			\App\Listeners\Provider\EditOffice\EditNumbers::class,
			\App\Listeners\Provider\AddNewOffice\SyncServices::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Provider\AddNewService::class => [
			\App\Listeners\Provider\AddNewService\InsertService::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Provider\EditService::class => [
			\App\Listeners\Provider\EditService\EditService::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Provider\AddNewEmployee::class => [
			\App\Listeners\Provider\AddNewEmployee\CreateUserIfNotExist::class,
			\App\Listeners\Provider\AddNewEmployee\InsertEmployee::class,
			\App\Listeners\Provider\AddNewEmployee\UpgradeToEmployee::class,
			\App\Listeners\Provider\AddNewEmployee\SyncEmployeePermission::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Provider\EditEmployee::class => [
			\App\Listeners\Provider\EditEmployee\UpdateEmployee::class,
			\App\Listeners\Provider\AddNewEmployee\SyncEmployeePermission::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Provider\AddNewSchedule::class => [
			\App\Listeners\Provider\AddNewSchedule\InsertSchedule::class,
			\App\Listeners\Provider\AddNewSchedule\InsertScheduleServices::class,
			\App\Listeners\LogMe::class
		],

		/**
		 * Web
		 */
		\App\Events\Web\ConvertRequest::class => [
			\App\Listeners\Web\ConvertRequest\InsertToProviderRequest::class,
			\App\Listeners\Web\ConvertRequest\UpdateUserInfo::class,
			\App\Listeners\LogMe::class
		],
		\App\Events\Web\UserRegister::class => [
			\App\Listeners\Web\UserRegister\SendAuthCodeSms::class,
			\App\Listeners\Web\UserRegister\CreateAuthCodeRecord::class,
			// \App\Listeners\LogMe::class
		],
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
	}
}
