<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Permission::all()->map(function ($permission) {
			Gate::define($permission->name, function ($user) use ($permission) {
				return $user->hasPermission($permission);
			});
		});

		Blade::if("role", function ($role) {
			return Auth::check() && User::find(Auth::id())->hasRole($role);
		});
	}
}
