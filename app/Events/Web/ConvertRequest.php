<?php

namespace App\Events\Web;

use App\Models\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConvertRequest
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_info;
    public $action_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $user_info)
    {
        $this->user_info	= $user_info;

        $this->action_id	= Log::getActionId(self::class);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
