<?php

namespace App\Events\Web;

use App\Models\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserRegister
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $mobileNumber;
	public $authCode;
	public $action_id;
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($mobileNumber)
	{
		$this->mobileNumber		= $mobileNumber;
		$this->authCode			= env("PRODUCTION") ? rand(1111, 9999) : 1234;

		if ($mobileNumber == "09194095098") {
			$this->authCode = env("PRODUCTION") ? 2211 : 1234;
		}

		$this->action_id		= Log::getActionId(self::class);
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}
}
