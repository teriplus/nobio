<?php

namespace App\Events\Admin;

use App\Models\Log;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AcceptToProvider
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public User $user;
    public $user_id;
    public $action_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, array $user_info)
    {
        $this->user			= User::find($user_id);
        $this->user_info	= $user_info;

        $this->action_id	= Log::getActionId(self::class);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
