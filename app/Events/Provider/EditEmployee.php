<?php

namespace App\Events\Provider;

use App\Models\Log;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EditEmployee
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $employee_info;
	public Employee $employee;
	public User $user;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Employee $employee, $employee_info)
	{
		$this->employee_info	= $employee_info;
		$this->employee			= $employee;
		$this->action_id		= Log::getActionId(self::class);
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}
}
