<?php

namespace App\Events\Provider;

use App\Models\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AddNewService
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $service_info;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $service_info)
    {
        $this->service_info = $service_info;

        $this->action_id = Log::getActionId(self::class);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
