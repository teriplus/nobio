<?php

namespace App\Events\Provider;

use App\Models\Log;
use App\Models\Office;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddNewSchedule
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public Office $office;
	public $schedule_info;
	public $action_id;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Office $office, $schedule_info)
	{
		$this->schedule_info	= $schedule_info;
		$this->office			= $office;
		$this->action_id		= Log::getActionId(self::class);
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}
}
