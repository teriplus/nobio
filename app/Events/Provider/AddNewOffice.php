<?php

namespace App\Events\Provider;

use App\Models\Log;
use App\Models\Office;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddNewOffice
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Office $office;

    public $office_info;
    public $action_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($office_info)
    {
        $this->office_info	= $office_info;

        $this->action_id	= Log::getActionId(self::class);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
