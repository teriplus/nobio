<?php

namespace App\Events\Provider;

use App\Models\Log;
use App\Models\Service;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EditService
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Service $service;
    public $service_info;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Service $service, array $service_info)
    {
        $this->service = $service;
        $this->service_info = $service_info;
        
        $this->action_id = Log::getActionId(self::class);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
